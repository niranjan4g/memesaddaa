-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 11, 2020 at 06:17 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `memesaddaa`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `username` varchar(225) NOT NULL,
  `password` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `username`, `password`) VALUES
(1, 'Niranjan', 'staradmin', '$2y$10$igtaSmtPZOA.kI2k5LUmE.p4MacisRrjyqK7ZXxJWlWIfFbWuSOYK'),
(2, 'Rituraj', 'rituraj', '$2y$10$TJBbvUHbwiQRA1hkBhcMd.YKDCyV9lrwNFg438cEx3kx0d2jgH3Su'),
(3, 'Sachin', 'sachin', '$2y$10$TJBbvUHbwiQRA1hkBhcMd.YKDCyV9lrwNFg438cEx3kx0d2jgH3Su'),
(4, 'Sam', 'sam', '$2y$10$TJBbvUHbwiQRA1hkBhcMd.YKDCyV9lrwNFg438cEx3kx0d2jgH3Su'),
(5, 'Bicky', 'bicky', '$2y$10$TJBbvUHbwiQRA1hkBhcMd.YKDCyV9lrwNFg438cEx3kx0d2jgH3Su'),
(6, 'Abhishek', 'slayer', '$2y$10$TJBbvUHbwiQRA1hkBhcMd.YKDCyV9lrwNFg438cEx3kx0d2jgH3Su'),
(7, 'Abhishek', 'dealer', '$2y$10$TJBbvUHbwiQRA1hkBhcMd.YKDCyV9lrwNFg438cEx3kx0d2jgH3Su');

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `likeid` int(11) NOT NULL,
  `meme_id` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `liked_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`likeid`, `meme_id`, `user_id`, `liked_on`) VALUES
(46, 'MEME00011', 'MEMEAD00002', '2020-07-09 06:24:44'),
(47, 'MEME00011', 'MEMEAD00001', '2020-07-09 08:04:09'),
(49, 'MEME00001', 'MEMEAD00001', '2020-07-10 03:15:48');

-- --------------------------------------------------------

--
-- Table structure for table `mememonth`
--

CREATE TABLE `mememonth` (
  `topid` int(11) NOT NULL,
  `meme_id` varchar(255) NOT NULL,
  `month` varchar(20) NOT NULL,
  `year` varchar(20) NOT NULL,
  `declared_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `memes`
--

CREATE TABLE `memes` (
  `memeid` int(11) NOT NULL,
  `meme_id` varchar(255) NOT NULL,
  `meme_caption` varchar(500) NOT NULL,
  `meme_content` varchar(500) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `posted_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `month` varchar(50) NOT NULL,
  `year` int(11) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `likes` bigint(20) NOT NULL,
  `updated_on` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `memes`
--

INSERT INTO `memes` (`memeid`, `meme_id`, `meme_caption`, `meme_content`, `user_id`, `posted_on`, `month`, `year`, `is_active`, `likes`, `updated_on`, `updated_by`) VALUES
(10, 'MEME00001', 'Dank meme', '140964825test.jpg', 'MEMEAD00001', '2020-07-07 08:35:49', 'July', 2020, 1, 1, '2020-07-11 03:20:37', '1'),
(11, 'MEME00011', 'Astronomia', '397908516maxresdefault.jpg', 'MEMEAD00002', '2020-07-07 08:36:51', 'July', 2020, 1, 2, '2020-07-11 03:18:39', '1');

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE `reports` (
  `reportid` int(11) NOT NULL,
  `meme_id` varchar(255) NOT NULL,
  `report_purpose` varchar(300) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `reason` varchar(1000) NOT NULL,
  `reported_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `action_taken` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_active` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reports`
--

INSERT INTO `reports` (`reportid`, `meme_id`, `report_purpose`, `user_id`, `reason`, `reported_on`, `action_taken`, `is_active`) VALUES
(2, 'MEME00011', 'Its Spam', 'MEMEAD00001', '', '2020-07-07 09:25:36', '2020-07-11 03:18:23', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userid` int(11) NOT NULL,
  `user_id` varchar(150) NOT NULL,
  `email` varchar(255) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `memername` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `password` varchar(500) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(255) NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` varchar(255) DEFAULT NULL,
  `pwd_link` varchar(500) DEFAULT NULL,
  `password_changed` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ipaddress` varchar(255) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '0',
  `profile_photo` varchar(255) NOT NULL,
  `profile_info` varchar(1000) NOT NULL,
  `verified` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userid`, `user_id`, `email`, `fullname`, `memername`, `phone`, `password`, `created_on`, `created_by`, `updated_on`, `updated_by`, `pwd_link`, `password_changed`, `ipaddress`, `is_active`, `profile_photo`, `profile_info`, `verified`) VALUES
(3, 'MEMEAD00001', 'niranjanpatra90@gmail.com', 'Niranjan Patra', 'niranjan_4g', '7978019601', '$2y$10$l.Fss4Z2Un7QVcyt3JYplOO3rR5/PUrWtR0QMHY0MN.w4SMWa9jGm', '2020-07-03 04:48:46', '', '2020-07-11 03:06:43', '1', '', '0000-00-00 00:00:00', '::1', 1, '1594010206_thump.jpg', '<p>Hello</p><p>I am Niranjan</p>', 0),
(4, 'MEMEAD00002', 'niranjanpatra2916@gmail.com', 'Niranjan', 'z0Th@n345', '9777788596', '$2y$10$jDoj0J7xF5.5RiUwvBQWvu3Vufn6CiCdk4Ab8dmlYPh7HPXNZjH4.', '2020-07-04 04:00:49', '', '2020-07-11 03:06:25', '1', NULL, '0000-00-00 00:00:00', '::1', 1, '', '', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`likeid`);

--
-- Indexes for table `mememonth`
--
ALTER TABLE `mememonth`
  ADD PRIMARY KEY (`topid`);

--
-- Indexes for table `memes`
--
ALTER TABLE `memes`
  ADD PRIMARY KEY (`memeid`);

--
-- Indexes for table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`reportid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `likeid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `mememonth`
--
ALTER TABLE `mememonth`
  MODIFY `topid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `memes`
--
ALTER TABLE `memes`
  MODIFY `memeid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `reports`
--
ALTER TABLE `reports`
  MODIFY `reportid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
