<?php 

include 'includes/config.php'; 
$targetDir = "content/memes/"; 
$watermarkImagePath = 'logo.png'; 
 
if($_SERVER["REQUEST_METHOD"] == "POST"){ 
    if(!empty($_FILES["meme_content"]["name"])){ 
        // File upload path 
        $fileName = basename($_FILES["meme_content"]["name"]); 
		$random = rand(0,999999999);
		$name_new = ($random.$fileName);
        $targetFilePath = $targetDir . $name_new; 
        $fileType = pathinfo($name_new,PATHINFO_EXTENSION); 
         
        // Allow certain file formats 
        $allowTypes = array('jpg','png','jpeg'); 
        if(in_array($fileType, $allowTypes)){ 
            // Upload file to the server 
            if(move_uploaded_file($_FILES["meme_content"]["tmp_name"], $targetFilePath)){ 
                // Load the stamp and the photo to apply the watermark to 
                $watermarkImg = imagecreatefrompng($watermarkImagePath); 
                switch($fileType){ 
                    case 'jpg': 
                        $im = imagecreatefromjpeg($targetFilePath); 
                        break; 
                    case 'jpeg': 
                        $im = imagecreatefromjpeg($targetFilePath); 
                        break; 
                    case 'png': 
                        $im = imagecreatefrompng($targetFilePath); 
                        break; 
                    default: 
                        $im = imagecreatefromjpeg($targetFilePath); 
                } 
                 
                // Set the margins for the watermark 
                $marge_right = 10; 
                $marge_bottom = 10; 
                 
                // Get the height/width of the watermark image 
                $sx = imagesx($watermarkImg); 
                $sy = imagesy($watermarkImg); 
                 
                // Copy the watermark image onto our photo using the margin offsets and  
                // the photo width to calculate the positioning of the watermark. 
                imagecopy($im, $watermarkImg, imagesx($im) - $sx - $marge_right, imagesy($im) - $sy - $marge_bottom, 0, 0, imagesx($watermarkImg), imagesy($watermarkImg)); 
                 
                // Save image and free memory 
                imagepng($im, $targetFilePath); 
                imagedestroy($im); 
				
				$meme_caption = $_POST['meme_caption'];
				$user_id = $_COOKIE['user'];
				$year = date('Y');
				$month = date('F');
				$meme_string = 'MEME';
				$target = $con->query("SELECT * FROM memes");
				$count = $target->fetch_assoc();
				$test = $count['memeid'];
				$meme_no = sprintf('%05d',$test+1);
				$meme_id = $meme_string.$meme_no;
				
                if(file_exists($targetFilePath)){ 
                    $sql = "INSERT INTO memes (meme_id, meme_caption, meme_content, user_id, year, month, posted_on) VALUES ('$meme_id', '$meme_caption', '$name_new', '$user_id', '$year', '$month', NOW())";
					if($con->query($sql) == TRUE){
						$_SESSION['success'] = 'Meme Posted Successfully!';
						header("location: index");
						exit();
					}else{
						$_SESSION['error'] = 'Something went wrong! Contact Admin';
						header("location: index");
						exit();
					}
					
                }else{ 
                    $_SESSION['error'] = "Image upload failed, please try again."; 
					header("location: index");
					exit();
                }  
            }else{ 
                $_SESSION['error'] = "Sorry, there was an error uploading your file."; 
				header("location: index");
				exit();
            } 
        }else{ 
            $_SESSION['error'] = 'Sorry, only JPG, JPEG, and PNG files are allowed to upload.'; 
			header("location: index");
			exit();
        } 
    }else{ 
        $_SESSION['error'] = 'Please select a file to upload.'; 
		header("location: index");
		exit();
    }
    
}else{
	$_SESSION['user'] = 'You are not Authorized!';
	header("location: index");
	exit();
}
 

?>