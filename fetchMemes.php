<?php

include 'includes/config.php';

if(isset($_POST['limit']))
{
 $query = "
 SELECT * FROM memes 
 WHERE is_active = 1
 ORDER BY memeid DESC 
 LIMIT ".$_POST["limit"]."
 ";

 $result = $con->query($query);

 

 

 $output = '';
if($result->num_rows === 0) {
	$output = 'No Memes Found';
	echo $output;
}else{
 while($row = $result->fetch_assoc())
 {
	 $user_id = $row['user_id'];
	 $record = $con->query("SELECT * FROM users WHERE user_id = '$user_id'")->fetch_assoc();
  $output .= '
  <div class="card">
      <div class="post-title d-flex align-items-center">                 
           <div class="profile-thumb">
                <a href="profile?user_id='.base64_encode(base64_encode(base64_encode($user_id))).'">
				';
				if($record["profile_photo"] == NULL) {
                 $output .=   '<figure class="profile-thumb-middle">
                        <img src="assets/user.png" alt="Default picture">
                    </figure>';
				} else {
				$output .= '<figure class="profile-thumb-middle">
                        <img src="profileimages/'.$record['profile_photo'].'" alt="profile picture">
                    </figure>';
				 } 
                $output .= '</a>
            </div>
            <div class="posted-author">
                <h6 class="author"><a href="profile?user_id='.base64_encode(base64_encode(base64_encode($user_id))).'">'.$record['memername'].'</a>';
				if($record['verified'] == 1) {
					
					$output .= '&nbsp;<i class="fa fa-check-circle" style="color: #07bbe8;"></i>';

					} 
				
				$output .= '</h6>
                <span class="post-time">'.date('d M, Y', strtotime($row['posted_on'])).'</span>
            </div>';
			
			if(isset($_COOKIE['user'])){ 
				if($user_id === $_COOKIE['user']) {
					$output .= '
			<div class="post-settings-bar">
				<span></span>
				<span></span>
				<span></span>
				<div class="post-settings arrow-shape">
					<ul>
						<li><a style="color: black; text-decoration: none;" href="delete-meme?meme_id='.base64_encode(base64_encode(base64_encode($row['meme_id']))).'">Delete Meme</a></li>
					</ul>
				</div>
			</div>';
			} else {
				$output .= '
			<div class="post-settings-bar">
				<span></span>
				<span></span>
				<span></span>
				<div class="post-settings arrow-shape">
					<ul>
						<li><button data-toggle="modal" onclick="reportmeme(\''.$row['meme_id'].'\')" data-target="#reportmeme" aria-disabled="true" id="reportmeme">Report</button></li>
					</ul>
				</div>
			</div>';
				
			} }
			$output .= '
		</div>
        <div class="post-content">
            <p class="post-desc">
                '.$row['meme_caption'].'
            </p>
            <div class="post-thumb-gallery">
                <figure class="post-thumb img-popup">
                   <img src="content/memes/'.$row['meme_content'].'" alt="Meme">
                </figure>
            </div>
            <div class="post-meta">';
			$meme_id = $row['meme_id'];
			if(isset($_COOKIE['user'])) {
				$usercheck = $_COOKIE['user'];
				$checklike = $con->query("SELECT * FROM likes WHERE user_id = '$usercheck' AND meme_id = '$meme_id'");
				if($checklike->num_rows === 0) {
					$likecount = $con->query("SELECT COUNT(*) as totallikes FROM likes where meme_id = '$meme_id'")->fetch_assoc();
					$output .= '<button class="post-meta-like likememe" id="'.$row['meme_id'].'" style="cursor: pointer;" onclick="likememe(\''.$row['meme_id'].'\', this.id)">
						<i class="fa fa-heart-o" style="color:red;"></i>
						<span>'.$likecount['totallikes'].' likes</span>
					</button>
					
					';
				}else{
					$likecount = $con->query("SELECT COUNT(*) as totallikes FROM likes where meme_id = '$meme_id'")->fetch_assoc();
					$output .= '<button class="post-meta-like likememe" id="'.$row['meme_id'].'" onclick="likememe(\''.$row['meme_id'].'\', this.id)" style="cursor: pointer;">
						<i class="fa fa-heart" style="color:red;"></i>
						<span>'.$likecount['totallikes'].' likes</span>
					</button>
					
					';
				}
			}else{
				$likecount = $con->query("SELECT COUNT(*) as totallikes FROM likes where meme_id = '$meme_id'")->fetch_assoc();
					$output .= '<a class="post-meta-like"  style="cursor: pointer;">
						
						<span>'.$likecount['totallikes'].' likes</span>
					</a>';
			}
			
			
                $output .=  '
            </div>
        </div>
    </div>
  ';
 }

 echo $output;
}
}

?>
