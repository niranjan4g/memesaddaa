<footer class="d-none d-lg-block">
        <div class="footer-area reveal-footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="footer-wrapper">
                            
                            <div class="card card-small mb-0 active-profile-wrapper" style="width: 100%; display: inline;">
								<span>Memesaddaa &copy; All Rights Reserved</span>
                                <span style="margin-left: 30%;">Made with <i class="fa fa-heart" style="color: red;"></i> in India</span>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    
    <!-- footer area end -->

    <!-- JS
============================================ -->

    <!-- Modernizer JS -->
    <script src="assets/js/vendor/modernizr-3.6.0.min.js"></script>
    <!-- jQuery JS -->
    <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
    <!-- Popper JS -->
    <script src="assets/js/vendor/popper.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="assets/js/vendor/bootstrap.min.js"></script>
    <!-- Slick Slider JS -->
    <script src="assets/js/plugins/slick.min.js"></script>
    <!-- nice select JS -->
    <script src="assets/js/plugins/nice-select.min.js"></script>
    <!-- audio video player JS -->
    <script src="assets/js/plugins/plyr.min.js"></script>
    <!-- perfect scrollbar js -->
    <script src="assets/js/plugins/perfect-scrollbar.min.js"></script>
    <!-- light gallery js -->
    <script src="assets/js/plugins/lightgallery-all.min.js"></script>
    <!-- image loaded js -->
    <script src="assets/js/plugins/imagesloaded.pkgd.min.js"></script>
    <!-- isotope filter js -->
    <script src="assets/js/plugins/isotope.pkgd.min.js"></script>
    <!-- Main JS -->
    <script src="assets/js/main.js"></script>
	
	<script>
  $(document).ready(function(){

    $('#dynamic_content').html(make_skeleton());

    setTimeout(function(){
     load_content(<?php
		$totalmemes = $con->query("SELECT * FROM memes WHERE is_active = 1");
		$limit = $totalmemes->num_rows;
		echo $limit;
	 ?>);
    }, 2000);

    function make_skeleton()
    {
      var output = '';
      for(var count = 0; count < <?php
		$totalmemes = $con->query("SELECT * FROM memes WHERE is_active = 1");
		$limit = $totalmemes->num_rows;
		echo $limit;
	 ?>; count++)
      {
        output += '<div class="ph-item">';
        output += '<div class="ph-col-2">';
        output += '<div class="ph-avatar">';
		output += '</div>';
		output += '</div>';
		output += '<div>';
        output += '<div class="ph-row">';
        output += '<div class="ph-col-4">';
		output += '</div>';
        output += '<div class="ph-col-8 empty">';
		output += '</div>';
        output += '<div class="ph-col-6">';
		output += '</div>';
        output += '<div class="ph-col-6 empty">';
		output += '</div>';
        output += '<div class="ph-col-2">';
		output += '</div>';
        output += '<div class="ph-col-10 empty">';
		output += '</div>';
        output += '</div>';
        output += '</div>';
        output += '<div class="ph-col-12">';
        output += '<div class="ph-picture">';
		output += '</div>';
        output += '<div class="ph-row">';
        output += '<div class="ph-col-10 big">';
		output += '</div>';
        output += '<div class="ph-col-2 empty big">';
		output += '</div>';
        output += '<div class="ph-col-4">';
		output += '</div>';
        output += '<div class="ph-col-8 empty">';
		output += '</div>';
        output += '<div class="ph-col-6">';
		output += '</div>';
        output += '<div class="ph-col-6 empty">';
		output += '</div>';
        output += '<div class="ph-col-12">';
		output += '</div>';
        output += '</div>';
        output += '</div>';
        output += '</div>';
      }
      return output;
    }

    function load_content(limit)
    {
      $.ajax({
        url:"fetchMemes.php",
        method:"POST",
        data:{limit:limit},
        success:function(data)
        {
          $('#dynamic_content').html(data);
        }
      })
    }

  });
</script>
<script type="text/javascript">
	function likememe(meme_id, id){
		
		$.ajax({
			url:"addLike.php",
			method:"POST",
			data: "meme_id="+meme_id,
			success:function(data){
				document.getElementById(id).innerHTML = data;
			},
			error:function(data){
				console.log(data);
			}
		});
	}
</script>
<script>
function reportmeme(meme_id){
	document.getElementById("report_id").value = meme_id;
}
</script>
<script src="ckeditor/ckeditor.js"></script>
<script>
       CKEDITOR.replace('profileinfo');
 </script>

</body>

</html>