<!-- widget single item start -->
                            <div class="card card-profile widget-item p-0">
                                <div class="profile-banner">
                                    <figure class="profile-banner-small">
                                        <a>
                                            <img src="assets/images/banner/banner-small.jpg" alt="">
                                        </a>
										<?php if(isset($_COOKIE['user'])) { ?>
                                        <a href="profile?user_id=<?php echo base64_encode(base64_encode(base64_encode($userdata['user_id']))); ?>" class="profile-thumb-2">
											<?php if($userdata['profile_photo'] == NULL) { ?>
                                            <img src="assets/user.png" alt="Default Picture">
											<?php } else { ?>
											<img src="profileimages/<?php echo $userdata['profile_photo']; ?>" alt="">
											<?php } ?>
                                        </a>
										<?php } else { ?>
										 <a class="profile-thumb-2">
                                            <img src="assets/user.png" alt="Guest Image">
                                        </a>
										<?php } ?>
                                    </figure>
                                    <div class="profile-desc text-center">
									<?php if(isset($_COOKIE['user'])){ ?>
                                        <h6 class="author"><a href="profile?user_id=<?php echo base64_encode(base64_encode(base64_encode($userdata['user_id']))); ?>"><?php echo $userdata['fullname']; ?></a>&nbsp;<?php if($userdata['verified'] == 1) { ?><i class="fa fa-check-circle" style="color: #07bbe8;"></i><?php } ?></h6>
										<?php if($userdata['profile_info'] == NULL) { ?>
                                        <p>Profile Info is not Updated!</p>
										<?php } else { ?>
										 <p><?php echo $userdata['profile_info']; ?></p>
										<?php } ?>
									<?php } else { ?>
										<h6 class="author"><a>Guest User</a></h6>
                                        <p>Hello! I am a Guest User.</p>
									<?php } ?>
                                    </div>
                                </div>
                            </div>
                            <!-- widget single item start -->