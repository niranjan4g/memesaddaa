<header>
        <div class="header-top sticky bg-white d-none d-lg-block">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-5">
                        <!-- header top navigation start -->
                        <div class="header-top-navigation">
                            <nav>
                                <ul>
                                    <li><a href="index">home</a></li>
									
                                </ul>
                            </nav>
                        </div>
                        <!-- header top navigation start -->
                    </div>

                    <div class="col-md-2">
                        <!-- brand logo start -->
                        <div class="brand-logo text-center">
                            <a href="index">
                                <img src="assets/images/logo/logo.png" alt="brand logo">
                            </a>
                        </div>
                        <!-- brand logo end -->
                    </div>

                    <div class="col-md-5">
					<?php if(isset($_COOKIE['user'])) {
						$user_id = $_COOKIE['user'];
						$userdata = $con->query("SELECT * FROM users WHERE user_id = '$user_id'")->fetch_assoc();
	
						?>
                        <div class="header-top-right d-flex align-items-center justify-content-end">
                           

                            <!-- profile picture start -->
                            <div class="profile-setting-box">
                                <div class="profile-thumb-small">
                                    <a href="javascript:void(0)" class="profile-triger">
									<?php
									if($userdata['profile_photo'] == NULL) {
									?>
                                        <figure>
                                            <img src="assets/user.png" alt="Default picture">
                                        </figure>
									<?php } else { ?>
										<figure>
                                            <img src="profileimages/<?php echo $userdata['profile_photo']; ?>" alt="profile picture">
                                        </figure>
									<?php } ?>
                                    </a>
                                    <div class="profile-dropdown">
                                        <div class="profile-head">
                                            <h5 class="name"><a><?php echo $userdata['memername']; ?>&nbsp;<?php if($userdata['verified'] == 1) { ?><i class="fa fa-check-circle" style="color: #07bbe8;"></i><?php } ?></a></h5>
                                            <a class="mail"><?php echo $userdata['email']; ?></a>
                                        </div>
                                        <div class="profile-body">
                                            <ul>
                                                <li><a href="profile?user_id=<?php echo base64_encode(base64_encode(base64_encode($userdata['user_id']))); ?>"><i class="fa fa-user"></i>Profile</a></li>
                                              
												<li><a href="leaderboard"><i class="fa fa-trophy"></i>Leader Board</a></li>
                                                <li><a href="logout"><i class="fa fa-power-off"></i>Sign out</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- profile picture end -->
                        </div>
					<?php } else { ?>
						<div class="header-top-right d-flex align-items-center justify-content-end">
                           

                            <!-- profile picture start -->
                            <div class="profile-setting-box">
                                <div class="profile-thumb-small">
                                    <a href="javascript:void(0)" class="profile-triger">
                                        <figure>
                                            <img src="assets/user.png" alt="Guest User">
                                        </figure>
                                    </a>
                                    <div class="profile-dropdown">
                                        <div class="profile-head">
                                            <h5 class="name"><a>Guest User</a></h5>
                                            <a class="mail">guestuser@memesaddaa.com</a>
                                        </div>
                                        <div class="profile-body">
                                            
                                            <ul>
												<li><a href="leaderboard"><i class="fa fa-trophy"></i>Leader Board</a></li>
                                                <li><a href="login"><i class="flaticon-unlock"></i>Login</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- profile picture end -->
                        </div>
					<?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </header>