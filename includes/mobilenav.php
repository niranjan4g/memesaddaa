<header>
        <div class="mobile-header-wrapper sticky d-block d-lg-none">
            <div class="mobile-header position-relative ">
                <div class="mobile-logo" style="background-color: white;">
                    <a href="index">
                        <img src="assets/images/logo/logo.png" alt="logo">
                    </a>
                </div>
                <?php if(isset($_COOKIE['user'])){ 
					$user_id = $_COOKIE['user'];
					$userdata = $con->query("SELECT * FROM users WHERE user_id = '$user_id'")->fetch_assoc();
				?>
					
                <div class="mobile-header-profile" style="background-color: white;"">
                    <!-- profile picture end -->
                    <div class="profile-thumb profile-setting-box">
                        <a href="javascript:void(0)" class="profile-triger">
						<?php if($userdata['profile_photo'] == NULL){ ?>
                            <figure class="profile-thumb-middle">
                                <img src="assets/user.png" alt="Default picture">
                            </figure>
						<?php } else { ?>
								<figure class="profile-thumb-middle">
                                <img src="profileimages/<?php echo $userdata['profile_photo']; ?>" alt="profile picture">
                            </figure>
						<?php } ?>
                        </a>
                        <div class="profile-dropdown text-left">
                            <div class="profile-head">
                                <h5 class="name"><a><?php echo $userdata['memername']; ?></a>&nbsp;<?php if($userdata['verified'] == 1) { ?><i class="fa fa-check-circle" style="color: #07bbe8;"></i><?php } ?></h5>
                                <a class="mail"><?php echo $userdata['email']; ?></a>
                            </div>
                            <div class="profile-body">
                                <ul>
                                    <li><a href="profile?user_id=<?php echo base64_encode(base64_encode(base64_encode($user_id))); ?>"><i class="fa fa-user"></i>Profile</a></li>
                                    <li><a href="leaderboard"><i class="fa fa-trophy"></i>Leader Board</i>
                                    <li><a href="logout"><i class="fa fa-power-off"></i>Sign out</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- profile picture end -->
                </div>
				<?php } else { ?>
				<div class="mobile-header-profile" style="background-color: white;">
                    <!-- profile picture end -->
                    <div class="profile-thumb profile-setting-box">
                        <a href="javascript:void(0)" class="profile-triger">
                            <figure class="profile-thumb-middle">
                                <img src="assets/user.png" alt="Guest picture">
                            </figure>
                        </a>
                        <div class="profile-dropdown text-left">
                            <div class="profile-head">
                                <h5 class="name"><a>Guest User</a></h5>
                                <a class="mail">guestuser@memesaddaa.com</a>
                            </div>
                            <div class="profile-body">
                                
                                <ul>
									<li><a href="leaderboard"><i class="fa fa-trophy"></i>Leader Board</a></li>
                                    <li><a href="login"><i class="flaticon-unlock"></i>Login</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- profile picture end -->
                </div>
				<?php } ?>
            </div>
        </div>
    </header>