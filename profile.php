<?php include 'includes/header.php'; ?>
<body>
<?php if(!isset($_GET['user_id'])){
	$user_id = $_COOKIE['user'];
	$result = $con->query("SELECT * FROM users WHERE user_id = '$user_id'");
	$ud = $result->fetch_assoc();
}else if(isset($_GET['user_id'])){
	$user_id = base64_decode(base64_decode(base64_decode($_GET['user_id'])));
	$result = $con->query("SELECT * FROM users WHERE user_id = '$user_id'");
	$ud = $result->fetch_assoc();
}else{
	$_SESSION['error'] = 'You are not Authorized';
	header("location: login");
	exit();
}
?>
<?php
        if(isset($_SESSION['error'])){
          echo "
           <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'error',
				layout: 'topRight',
				text: '".$_SESSION['error']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'success',
				layout: 'topRight',
				text: '".$_SESSION['success']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['success']);
        }
      ?>
    <?php include 'includes/brandbar.php'; ?>
    
	<?php include 'includes/mobilenav.php'; ?>

    <main>

        <div class="main-wrapper">
            <!-- profile banner area start -->
			
            <div class="profile-banner-large bg-img" data-bg="assets/images/banner/profile-banner.jpg">
            </div>
            <!-- profile banner area end -->

            <!-- profile menu area start -->
            <div class="profile-menu-area secondary-navigation-style bg-white">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-3 col-md-3">
                            <div class="profile-picture-box">
                                <figure class="profile-picture">
								<?php if($ud['profile_photo'] == NULL) { ?>
                                    <a>
                                        <img style="border: 12px solid black;" src="assets/user.png" alt="Default picture">
                                    </a>
								<?php } else { ?>
									<a>
                                        <img style="border: 12px solid black;" src="profileimages/<?php echo $ud['profile_photo']; ?>" alt="profile picture">
                                    </a>
								<?php } ?>
                                </figure>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 offset-lg-1">
                            <div class="profile-menu-wrapper">
                                <div class="main-menu-inner header-top-navigation">
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-3 d-none d-md-block">
                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- profile menu area end -->

            <!-- sendary menu start -->
            <div class="menu-secondary">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="secondary-menu-wrapper bg-white">
                                <div class="page-title-inner">
                                    <h4 class="page-title">about</h4>
                                </div>
                                <div class="main-menu-inner header-top-navigation">
                                    <nav>
                                        <ul class="main-menu" style="color: white;">
                                            <li><a><span></span> <i class="flaticon-facebook"></i></a></li>
                                            <li><a><span></span> <i class="flaticon-twitter-logo-silhouette"></i></a></li>
                                            <li><a><span></span> <i class="fa fa-youtube-play"></i></a></li>
                                            <li><a><span></span> <i class="fa fa-instagram"></i></a></li>
                                           
                                        </ul>
                                    </nav>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- sendary menu end -->

            <!-- about author details start -->
            <div class="about-author-details">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="card widget-item">
                               
								<?php
								if(isset($_COOKIE['user'])){
									if(isset($_GET['user_id'])){
										if(base64_decode(base64_decode(base64_decode($_GET['user_id']))) === $_COOKIE['user']){
								?>
								 <div class="about-me">
                                    <ul class="nav flex-column about-author-menu">
                                        <li><a href="#one" data-toggle="tab" class="active">About Me</a></li>
                                        <li><a href="#two" data-toggle="tab">Edit Profile</a></li>
                                        <li><a href="#three" data-toggle="tab">Change Password</a></li>
                                        <li><a href="#four" data-toggle="tab">Change Profile Picture</a></li>
										<li><a href="logout">Logout</a></li>
										</ul>
                                </div>
								<?php } } } ?>   
                                    
                            </div>
                        </div>
                        <div class="col-lg-9">
                            <div class="about-description">
							    <div class="tab-content">
								<div class="tab-pane fade active show" id="one">
                                        <div class="work-zone">
                                            <div class="author-desc-title d-flex">
                                                <h6 class="author"><?php echo $ud['fullname']; ?>&nbsp;<?php if($ud['verified'] == 1) { ?><i class="fa fa-check-circle" style="color: #07bbe8;"></i><?php } ?></h6>
                                                
                                            </div>
                                            <?php if($ud['profile_info'] != NULL){ ?>
											<p><?php echo $ud['profile_info']; ?></p>
											<?php } else { ?>
											<p>Profile Info Not Updated!</p>
											<?php } ?>
                                        </div>
                                    </div>
							<?php
								if(isset($_COOKIE['user'])){
									if(isset($_GET['user_id'])){
										if(base64_decode(base64_decode(base64_decode($_GET['user_id']))) === $_COOKIE['user']){
								?>

                                    
                                    <div class="tab-pane fade" id="two">
                                        <div class="work-zone">
                                            <div class="author-desc-title d-flex">
                                                <h6 class="author">Edit Profile</h6>
                                                
                                            </div>
                                           <form action="edit-profile" method="post">
										   <div class="form-group">
											<label id="fullname">Full Name</label>
											<input type="text" name="fullname" id="fullname" placeholder="Enter Full Name" class="form-control" required value="<?php echo $ud['fullname']; ?>">
										   </div>
										   <div class="form-group">
											<label id="phone">Phone Number</label>
											<input type="text" name="phone" id="phone" placeholder="Enter Phone Number" class="form-control" required value="<?php echo $ud['phone']; ?>">
										   </div>
										    <div class="form-group">
											<label>Profile Info</label>
											<textarea name="profile_info" id="profileinfo" placeholder="Enter Profile Info" class="form-control" required ><?php echo $ud['profile_info']; ?></textarea>
										   </div>
										   <div class="form-group">
											<button type="submit" class="post-share-btn">Update Profile</button>
										   </div>
										   </form>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="three">
                                        <div class="work-zone">
                                            <div class="author-desc-title d-flex">
                                                <h6 class="author">Change Password</h6>
                                                
                                            </div>
                                            <form action="change-password" method="post">
										   <div class="form-group">
											<label id="currentpassword">Current Password</label>
											<input type="password" name="cpass" id="currentpassword" placeholder="Enter Current Password" class="form-control" required >
										   </div>
										   <div class="form-group">
											<label id="newpassword">New Password</label>
											<input type="password" name="npass" id="newpassword" placeholder="Enter New Password" class="form-control" required>
										   </div>
										    <div class="form-group">
											<label id="rpassword">Re-Type New Password</label>
										<input type="password" name="rpass" id="rpassword" placeholder="Re-Type New Password" class="form-control" required>
										   </div>
										   <div class="form-group">
											<button type="submit" class="post-share-btn">Change Password</button>
										   </div>
										   </form>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="four">
                                        <div class="work-zone">
                                            <div class="author-desc-title d-flex">
                                                <h6 class="author">Change Profile Picture</h6>
                                                
                                            </div>
                                           <form action="change-picture" method="post" enctype="multipart/form-data">
											 <div class="form-group">
												<label>Upload New Profile Picture</label>
												<input type="file" name="profile_photo" class="form-control" required>
											 </div>
											 <div class="form-group">
												
												<button type="submit" class="post-share-btn">Change Profile Picture</button>
											 </div>
											</form>
                                        </div>
                                    </div>
                                    
                                
								<?php } } } ?>   
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- about author details start -->

            <!-- photo section start -->
            <div class="photo-section">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-box">
                                <h5 class="content-title">Memes</h5>
                                <div class="content-body">
                                    <div class="row mt--30">
									<?php
									if(!isset($_GET['user_id'])){
										$_SESSION['error'] = 'Please Select a Memer';
										header("location: index");
										exit();
									}else{
										$user_id = base64_decode(base64_decode(base64_decode($_GET['user_id'])));
										$rem = $con->query("SELECT * FROM memes WHERE user_id = '$user_id' AND is_active = 1");
										while($array = $rem->fetch_assoc()){
										$meme_id = $array['meme_id'];
									?>
									
                                        <div class="col-sm-6 col-md-4">
                                            <div class="photo-group">
                                                <div class="gallery-toggle">
                                                    <div class="d-none product-thumb-large-view">
                                                        <img src="content/memes/<?php echo $array['meme_content']; ?>" alt="Meme">
                                                        
                                                    </div>
                                                    <div class="gallery-overlay">
                                                        <img src="content/memes/<?php echo $array['meme_content']; ?>" alt="meme">
                                                        <div class="view-icon"></div>
                                                    </div>
                                                    <div class="photo-gallery-caption">
                                                        <h3 class="photos-caption"><?php echo $array['meme_caption'] ;?></h3>
                                                    </div>
                                                </div>
                                            </div>
											<center>
											<div>
											<?php if(isset($_COOKIE['user'])) {
										$usercheck = $_COOKIE['user'];
										$checklike = $con->query("SELECT * FROM likes WHERE user_id = '$usercheck' AND meme_id = '$meme_id'");
										if($checklike->num_rows === 0) {
											$likecount = $con->query("SELECT COUNT(*) as totallikes FROM likes where meme_id = '$meme_id'")->fetch_assoc();
											?>
											<button class="post-meta-like likememe" id="<?php echo $meme_id; ?>" style="cursor: pointer;" onclick="likememe('<?php echo $meme_id; ?>', this.id)">
												<i class="fa fa-heart-o" style="color:red;"></i>
												<span><?php echo $likecount['totallikes']; ?> likes</span>
											</button>
											
											<?php
										}else{
											$likecount = $con->query("SELECT COUNT(*) as totallikes FROM likes where meme_id = '$meme_id'")->fetch_assoc();
											?> <button class="post-meta-like likememe" id="<?php echo $meme_id; ?>" onclick="likememe('<?php echo $meme_id; ?>', this.id)" style="cursor: pointer;">
												<i class="fa fa-heart" style="color:red;"></i>
												<span><?php echo $likecount['totallikes']; ?> likes</span>
											</button>
											
											<?php
										}
									}else{
										$likecount = $con->query("SELECT COUNT(*) as totallikes FROM likes where meme_id = '$meme_id'")->fetch_assoc();
											?> <a class="post-meta-like"  style="cursor: pointer;">
												
												<span><?php echo $likecount['totallikes']; ?> likes</span>
											</a>
									<?php } ?>
									</div>
									</center>
                                        </div>
										
                                        
									<?php } } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- photo section end -->

            

            
        </div>

    </main>

    <!-- Scroll to top start -->
    <div class="scroll-top not-visible">
        <i class="bi bi-finger-index"></i>
    </div>

<?php include 'includes/footer.php'; ?>