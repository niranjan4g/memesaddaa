<?php
include 'includes/config.php';
if(isset($_COOKIE['user'])){
	if(isset($_GET['meme_id'])){
		$meme_id = base64_decode(base64_decode(base64_decode($_GET['meme_id'])));
		$res = $con->query("SELECT * FROM memes WHERE meme_id = '$meme_id'");
		if($res->num_rows === 0){
			$_SESSION['error'] = 'Sorry the operation could not be performed!';
			header("location: index");
			exit();
		}else{
			$sql = "DELETE FROM memes WHERE meme_id = '$meme_id'";
			if($con->query($sql) == TRUE){
				$con->query("DELETE FROM likes WHERE meme_id = '$meme_id'");
				$_SESSION['success'] = 'Meme Successfully Deleted!';
				header("location: index");
				exit();
			}else{
				$_SESSION['error'] = 'Something Went Wrong! Contact Admin';
				header("location: index");
				exit();
			}
		}
	}else{
		$_SESSION['error'] = 'Sorry the operation could not be performed!';
		header("location: index");
		exit();
	}
}else{
	$_SESSION['error'] = 'You are not Authorized!';
	header("location: index");
	exit();
}
?>