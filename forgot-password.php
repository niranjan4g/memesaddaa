<?php include 'includes/header.php'; ?>
<?php
if(isset($_COOKIE['user'])){
	$_SESSION['success'] = 'Logged In Successfully';
	header("location: index");
	exit();
}
?>
<body class="bg-transparent">
<?php
        if(isset($_SESSION['error'])){
          echo "
           <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'error',
				layout: 'topRight',
				text: '".$_SESSION['error']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'success',
				layout: 'topRight',
				text: '".$_SESSION['success']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['success']);
        }
      ?>
    <main>
        <div class="main-wrapper pb-0 mb-0">
            <div class="timeline-wrapper">
                <div class="timeline-header">
                    <div class="container-fluid p-0" style="margin-top: 40px;">
                        <div class="row">
                            <div class="col-lg-12">
                               
									<center>
                                        <a href="index">
                                            <img src="assets/images/logo/logo.png" alt="timeline logo">
                                        </a>
									</center>
                                   
                            </div>
                            
                    </div>
                </div>
                <div class="timeline-page-wrapper" style="margin-top: 80px;">
                    <div class="container-fluid p-0">
                        <div class="row no-gutters">
                           
                            <div class="col-lg-12 order-1 order-lg-2 d-flex align-items-center justify-content-center">
                                <div class="signup-form-wrapper">
                                   
                                    <div class="signup-inner text-center">
                                        <h3 class="title">Forgot Password</h3>
                                        <form class="signup-inner--form" method="post" action="send-link">
                                            <div class="row">
                                                <div class="col-12">
                                                    <input type="email" name="email" id="email" class="single-field" placeholder="Email" required>
													
                                                </div>
												
                                                
                                               
                                                <div class="col-12">
                                                    <button class="submit-btn">Send Reset Link</button>
                                                </div>
                                            </div>
                                            
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <!-- JS
============================================ -->

    <!-- Modernizer JS -->
    <script src="assets/js/vendor/modernizr-3.6.0.min.js"></script>
    <!-- jQuery JS -->
    <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
    <!-- Popper JS -->
    <script src="assets/js/vendor/popper.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="assets/js/vendor/bootstrap.min.js"></script>
    <!-- Slick Slider JS -->
    <script src="assets/js/plugins/slick.min.js"></script>
    <!-- nice select JS -->
    <script src="assets/js/plugins/nice-select.min.js"></script>
    <!-- audio video player JS -->
    <script src="assets/js/plugins/plyr.min.js"></script>
    <!-- perfect scrollbar js -->
    <script src="assets/js/plugins/perfect-scrollbar.min.js"></script>
    <!-- light gallery js -->
    <script src="assets/js/plugins/lightgallery-all.min.js"></script>
    <!-- image loaded js -->
    <script src="assets/js/plugins/imagesloaded.pkgd.min.js"></script>
    <!-- isotope filter js -->
    <script src="assets/js/plugins/isotope.pkgd.min.js"></script>
    <!-- Main JS -->
    <script src="assets/js/main.js"></script>
	
</body>

</html>