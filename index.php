<?php include 'includes/header.php'; ?>
<body>
<?php if(isset($_COOKIE['user'])){
	$user_id = $_COOKIE['user'];
	$result = $con->query("SELECT * FROM users WHERE user_id = '$user_id'");
	$userdata = $result->fetch_assoc();
}
?>
<?php
        if(isset($_SESSION['error'])){
          echo "
           <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'error',
				layout: 'topRight',
				text: '".$_SESSION['error']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'success',
				layout: 'topRight',
				text: '".$_SESSION['success']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['success']);
        }
      ?>
    <!-- header area start -->
    <?php include 'includes/brandbar.php'; ?>
    <!-- header area end -->
    <!-- header area start -->
    <?php include 'includes/mobilenav.php'; ?>
    <!-- header area end -->

    <main>

        <div class="main-wrapper pt-80">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 order-2 order-lg-1">
                        <aside class="widget-area">
                            <?php include 'includes/profile-card.php'; ?>

                            <!-- widget single item start -->
                            <div class="card widget-item">
                                <h4 class="widget-title">Post your Memes and you may Win!</h4>
                                <div class="widget-body">
                                    <img src="assets/images/reward.jpg" />
                                </div>
								<span style="text-align: right;"><span style="color: red;">*</span>T&C's  applied</span>
                            </div>
                            <!-- widget single item end -->

                        </aside>
                    </div>

                    <div class="col-lg-6 order-1 order-lg-2">
                        <!-- share box start -->
					<?php if(isset($_COOKIE['user'])) { ?>
                        <div class="card card-small">
                            <div class="share-box-inner">
                                <!-- profile picture end -->
                                <div class="profile-thumb">
                                    <a>
									<?php if($userdata['profile_photo'] == NULL) { ?>
                                        <figure class="profile-thumb-middle">
                                            <img src="assets/user.png" alt="Default picture">
                                        </figure>
									<?php } else { ?>
										  <figure class="profile-thumb-middle">
                                            <img src="profileimages/<?php echo $userdata['profile_photo']; ?>" alt="profile picture">
                                        </figure>
									<?php } ?>
                                    </a>
                                </div>
                                <!-- profile picture end -->

                                <!-- share content box start -->
                                <div class="share-content-box w-100">
                                    <form class="share-text-box">
                                        <textarea name="share" class="share-text-field" aria-disabled="true" placeholder="Drop your Meme" data-toggle="modal" data-target="#textbox" id="email" readonly></textarea>
                                        <button class="btn-share" type="submit" disabled>share</button>
                                    </form>
                                </div>
                                <!-- share content box end -->

                                <!-- Modal start -->
                                <div class="modal fade" id="textbox" aria-labelledby="textbox">
                                    <div class="modal-dialog">
									<form action="add-meme" method="post" enctype="multipart/form-data">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Drop Your Meme</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body custom-scroll">
                                                <textarea name="meme_caption" class="share-field-big custom-scroll" placeholder="Caption" required></textarea>
                                            </div>
											<label style="margin-left: 3%; font-weight: bold;">Upload Meme</label>
											<div class="modal-body custom-scroll">
												<input type="file" name="meme_content" required accept=".jpg, .png, .jpeg">
											</div>
                                            <div class="modal-footer">
                                                <button type="button" class="post-share-btn" data-dismiss="modal">cancel</button>
                                                <button type="submit" class="post-share-btn">post</button>
                                            </div>

                                        </div>
									</form>
                                    </div>
                                </div>
								   <div class="modal fade" id="reportmeme" aria-labelledby="reportmeme">
                                    <div class="modal-dialog">
									<form action="report-meme" method="post">
                                        <div class="modal-content">
										
                                            <div class="modal-header">
                                                <h5 class="modal-title">Report Meme</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
											
											<label style="margin-left: 3%; font-weight: bold;">Upload Meme</label>
											<div class="modal-body form-group">
												<select name="report_purpose" class="form-control" required>
													<option selected disabled>--Select Reason--</option>
													<option value="Its Spam">Its Spam</option>
													<option value="Nudity or Sexual Activity">Nudity or Sexual Activity</option>
													<option value="Hate Speech or symbols">Hate Speech or symbols</option>
													<option value="Violence or dangerous organizations">Violence or dangerous organizations</option>
													<option value="Sale of ilegal or regulated goods">Sale of ilegal or regulated goods</option>
													<option value="Bullying or harassment">Bullying or harassment</option>
													<option value="Intellectual property violation">Intellectual property violation</option>
													<option value="Suicide, self-injury or eating disorders">Suicide, self-injury or eating disorders</option>
													<option value="Scam or Fraud">Scam or Fraud</option>
													<option value="False Information">False Information</option>
													<option value="I just don't like it">I just don't like it</option>
												</select>
											</div>
											<input type="hidden" name="meme_id" id="report_id" required>
                                            <div class="modal-body custom-scroll">
                                                <textarea name="reason" class="share-field-big custom-scroll" placeholder="Describe Your Reason to Report"></textarea>
                                            </div>
											
                                            <div class="modal-footer">
                                                <button type="button" class="post-share-btn" data-dismiss="modal">cancel</button>
                                                <button type="submit" class="post-share-btn">post</button>
                                            </div>

                                        </div>
									</form>
                                    </div>
                                </div>
                                <!-- Modal end -->
                            </div>
                        </div>
					<?php } ?>
                     <div id="dynamic_content">
                       
                     </div>  
                        
                        

                    </div>

                    <div class="col-lg-3 order-3">
                        <aside class="widget-area">
                            <!-- widget single item start -->
                            <div class="card widget-item">
                                <h4 class="widget-title">New Memers</h4>
                                <div class="widget-body">
                                    <ul class="like-page-list-wrapper">
									
									<?php
										if(isset($_COOKIE['user'])){
												$user_id = $_COOKIE['user'];
												
												$res = $con->query("SELECT user_id, profile_photo, memername, verified, created_on FROM users WHERE is_active = 1 AND NOT user_id = '$user_id' ORDER BY userid DESC LIMIT 10");
												
										}else{
											$res = $con->query("SELECT user_id, profile_photo, memername, verified, created_on FROM users WHERE is_active = 1 ORDER BY userid DESC LIMIT 10");
										}
												while($arrange = $res->fetch_assoc()) {
										
									?>
                                        <li class="unorder-list">
                                            <!-- profile picture end -->
                                            <div class="profile-thumb">
                                                <a href="profile?user_id=<?php echo base64_encode(base64_encode(base64_encode($arrange['user_id']))); ?>">
												<?php
													if($arrange['profile_photo'] == NULL) {
												?>
                                                    <figure class="profile-thumb-small">
                                                        <img src="assets/user.png" alt="Default picture">
                                                    </figure>
													<?php } else { ?>
													 <figure class="profile-thumb-small">
                                                        <img src="profileimages/<?php echo $arrange['profile_photo']; ?>" alt="profile picture">
                                                    </figure>
													<?php } ?>
                                                </a>
                                            </div>
                                            <!-- profile picture end -->

                                            <div class="unorder-list-info">
                                                <h3 class="list-title"><a href="profile?user_id=<?php echo base64_encode(base64_encode(base64_encode($arrange['user_id']))); ?>"><?php echo $arrange['memername']; ?></a>&nbsp;<?php if($arrange['verified'] == 1) { ?><i class="fa fa-check-circle" style="color: #07bbe8;"></i><?php } ?></h3>
                                                <p class="list-subtitle">Joined on: <?php echo date('d F,Y', strtotime($arrange['created_on'])); ?></p>
                                            </div>
                                        </li>
										
										<?php } ?>	
                                        
                                    </ul>
                                </div>
                            </div>
                            <!-- widget single item end -->

                            <!-- widget single item start -->
                            <div class="card widget-item">
                                <h4 class="widget-title">Advertisement</h4>
                                <div class="widget-body">
                                    <div class="add-thumb">
                                        <p>Contact to get your Brand Advertised</p>
                                    </div>
                                </div>
                            </div>
                            <!-- widget single item end -->

                            <!-- widget single item start -->
                            <div class="card widget-item">
                                <h4 class="widget-title">Top Memes</h4>
                                <div class="widget-body">
                                    <ul class="like-page-list-wrapper">
									<?php
									$ress = $con->query("SELECT meme_content, user_id, likes FROM memes WHERE is_active = 1 ORDER BY likes DESC, posted_on ASC LIMIT 5");
									while($arr = $ress->fetch_assoc()) {
										$user_id = $arr['user_id'];
										$memername = $con->query("SELECT memername, verified FROM users WHERE user_id = '$user_id'")->fetch_assoc();
									?>
                                        <li class="unorder-list">
                                            <!-- profile picture end -->
                                            <div class="profile-thumb">
                                                
                                                    <figure class="profile-thumb-small">
                                                        <img src="content/memes/<?php echo $arr['meme_content']; ?>" alt="MEME">
                                                    </figure>
                                                
                                            </div>
                                            <!-- profile picture end -->

                                            <div class="unorder-list-info">
                                                <h3 class="list-title"><a href="profile?user_id=<?php echo base64_encode(base64_encode(base64_encode($arr['user_id']))); ?>"><?php echo $memername['memername']; ?><?php if($memername['verified'] == 1) { ?>&nbsp;<i class="fa fa-check-circle" style="color: #07bbe8;"></i><?php } ?></a></h3>
                                                <p class="list-subtitle"><i style="color: red;" class="fa fa-heart"></i>&nbsp;<?php echo $arr['likes']; ?> Likes</p>
                                            </div>
                                           
                                        </li>
                                      <?php
										}
									  ?>
                                    </ul>
                                </div>
                            </div>
                            <!-- widget single item end -->
                        </aside>
                    </div>
                </div>
            </div>
        </div>

    </main>

    <!-- Scroll to top start -->
    <div class="scroll-top not-visible">
        <i class="bi bi-finger-index"></i>
    </div>

 <?php include 'includes/footer.php'; ?>