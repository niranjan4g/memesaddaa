<?php include 'includes/header.php'; ?>
<?php
if(isset($_COOKIE['user'])){
	$_SESSION['success'] = 'Logged In Successfully';
	header("location: index");
	exit();
}
?>
<body class="bg-transparent">
<?php
        if(isset($_SESSION['error'])){
          echo "
           <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'error',
				layout: 'topRight',
				text: '".$_SESSION['error']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'success',
				layout: 'topRight',
				text: '".$_SESSION['success']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['success']);
        }
      ?>
    <main>
        <div class="main-wrapper pb-0 mb-0">
            <div class="timeline-wrapper">
                <div class="timeline-header">
                    <div class="container-fluid p-0">
                        <div class="row no-gutters align-items-center">
                            <div class="col-lg-6">
                                <div class="timeline-logo-area d-flex align-items-center">
                                    <div class="timeline-logo">
                                        <a href="index">
                                            <img src="assets/images/logo/logo.png" alt="timeline logo">
                                        </a>
                                    </div>
                                    <div class="timeline-tagline">
                                        <h6 class="tagline" style="color: white;">It’s helps you to connect and share with the Memers in the World</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="login-area">
								<form action="login-user" method="post">
                                    <div class="row align-items-center">
                                        <div class="col-12 col-sm">
                                            <input type="email" name="email" placeholder="Email" class="single-field" required>
                                        </div>
                                        <div class="col-12 col-sm">
                                            <input type="password" name="password" placeholder="Password" class="single-field" required>
                                        </div>
                                        <div class="col-12 col-sm-auto">
                                            <button type="submit" class="login-btn">Login</button>
                                        </div>
                                    </div>
								</form>
								<a href="forgot-password" style="color: white; text-decoration: none;">Forgot password?</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="timeline-page-wrapper">
                    <div class="container-fluid p-0">
                        <div class="row no-gutters">
                            <div class="col-lg-6 order-2 order-lg-1">
                                <div class="timeline-bg-content bg-img" data-bg="assets/images/collage.jpg">
                                    <h3 class="timeline-bg-title">See some Dank Memes around the World. Welcome to MemesAddaa.</h3>
                                </div>
                            </div>
                            <div class="col-lg-6 order-1 order-lg-2 d-flex align-items-center justify-content-center">
                                <div class="signup-form-wrapper">
                                    <h1 class="create-acc text-center">Create An Account</h1>
                                    <div class="signup-inner text-center">
                                        <h3 class="title">Wellcome to MemesAddaa</h3>
                                        <form class="signup-inner--form" method="post" action="create-user">
                                            <div class="row">
                                                <div class="col-12">
                                                    <input type="email" name="email" id="email" class="single-field" placeholder="Email" required>
													<div id="email-error"style="position: absolute; top: 40px;"></div>
                                                </div>
												
                                                <div class="col-md-6">
                                                    <input type="text" name="fullname" id="fullname" class="single-field" placeholder="Full Name" required>
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" name="phone" id="phone" class="single-field" placeholder="Contact Number" required>
                                                </div>
                                                <div class="col-12">
                                                    <input type="text" name="memername" id="memername" class="single-field" placeholder="Memer Name" required>
													<div id="user-error"style="position: absolute; top: 40px;"></div>
                                                </div>
												
                                                <div class="col-md-12">
                                                    <input type="password" name="pass" id="pass" class="single-field" placeholder="Enter Password" required>
                                                </div>
                                                <div class="col-md-12">
                                                     <input type="password" name="cpass" id="cpass" class="single-field" placeholder="Confirm Password" required>
                                                </div>
                                               
                                                <div class="col-12">
                                                    <button class="submit-btn">Create Account</button>
                                                </div>
                                            </div>
                                            <h6 class="terms-condition">I have read & accepted the <a href="#">terms of use</a></h6>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <!-- JS
============================================ -->

    <!-- Modernizer JS -->
    <script src="assets/js/vendor/modernizr-3.6.0.min.js"></script>
    <!-- jQuery JS -->
    <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
    <!-- Popper JS -->
    <script src="assets/js/vendor/popper.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="assets/js/vendor/bootstrap.min.js"></script>
    <!-- Slick Slider JS -->
    <script src="assets/js/plugins/slick.min.js"></script>
    <!-- nice select JS -->
    <script src="assets/js/plugins/nice-select.min.js"></script>
    <!-- audio video player JS -->
    <script src="assets/js/plugins/plyr.min.js"></script>
    <!-- perfect scrollbar js -->
    <script src="assets/js/plugins/perfect-scrollbar.min.js"></script>
    <!-- light gallery js -->
    <script src="assets/js/plugins/lightgallery-all.min.js"></script>
    <!-- image loaded js -->
    <script src="assets/js/plugins/imagesloaded.pkgd.min.js"></script>
    <!-- isotope filter js -->
    <script src="assets/js/plugins/isotope.pkgd.min.js"></script>
    <!-- Main JS -->
    <script src="assets/js/main.js"></script>
	<script type="text/javascript">
			$(document).ready(function() {
				$('#email').on('change', function() {
					var email = $(this).val();
					
					if(email){
						$.ajax({
							type:'POST',
							url:'checkEmail.php',
							data:'email='+email,
							success:function(data){
								$('#email-error').html(data);
							}
						}); 
					}
				});
			});
		</script>
		<script type="text/javascript">
			$(document).ready(function() {
				$('#memername').on('change', function() {
					var memername = $(this).val();
					
					if(email){
						$.ajax({
							type:'POST',
							url:'checkMemer.php',
							data:'memername='+memername,
							success:function(data){
								$('#user-error').html(data);
							}
						}); 
					}
				});
			});
		</script>

</body>

</html>