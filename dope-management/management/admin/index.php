<?php include 'header.php'; ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->

      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Dashboard</h3>
                <div class="card-tools">
                 
                  <div class="input-group input-group-sm">
                  
                  
                  </div>
               
                </div>
              </div>
              <!-- /.card-header -->
			  <div class="card-body">
					 <div class="row">
						 <div class="col-sm-3">
						<div class="card text-white bg-success mb-3">
						
						  <div class="card-body">
							<h5 class="card-title">Total Users</h5>
							<?php
							$countusers = $con->query("SELECT COUNT(*) as totalusers FROM users")->fetch_assoc();
							?>
							<p class="card-text" style="font-size: 40px; font-weight: bold;"><?php echo $countusers['totalusers']; ?></p>
							
						  </div>
						 
						<a href="all_users" style="text-align: right; padding: 10px;" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
						</div>
					  </div>
					   <div class="col-sm-3">
						<div class="card text-white bg-primary mb-3">
						
						  <div class="card-body">
							<h5 class="card-title">Total Memes</h5>
							<?php
							$countmemes = $con->query("SELECT COUNT(*) as totalmemes FROM memes")->fetch_assoc();
							?>
												<p class="card-text" style="font-size: 40px; font-weight: bold;"><?php echo $countmemes['totalmemes']; ?></p>
							
						  </div>
						 
						<a href="all_memes" style="text-align: right; padding: 10px;" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
						</div>
					  </div>
					   <div class="col-sm-3">
						<div class="card text-white bg-danger mb-3">
						
						  <div class="card-body">
							<h5 class="card-title">Total Likes</h5>
							<?php
							$countlikes = $con->query("SELECT COUNT(*) as totallikes FROM likes")->fetch_assoc();
							?>
												<p class="card-text" style="font-size: 40px; font-weight: bold;"><?php echo $countlikes['totallikes']; ?></p>
							
						  </div>
						 
						<a href="top_meme" style="text-align: right; padding: 10px;" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
						</div>
					  </div>
					   <div class="col-sm-3">
						<div class="card text-white bg-warning mb-3" >
						
						  <div class="card-body" style="color: white;">
							<h5 class="card-title">Total Reports</h5>
							<?php
							$countreports = $con->query("SELECT COUNT(*) as totalreports FROM reports")->fetch_assoc();
							?>
												<p class="card-text" style="font-size: 40px; font-weight: bold;"><?php echo $countreports['totalreports']; ?></p>
							
						  </div>
						 
						<a href="reports" style="text-align: right; padding: 10px; color: white !important;" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
						</div>
					  </div>
					  <div class="col-sm-3">
						<div class="card text-white bg-info mb-3" >
						
						  <div class="card-body" style="color: white;">
							<h5 class="card-title">Total Verified Users</h5>
							<?php
							$countverified = $con->query("SELECT COUNT(*) as totalverified FROM users WHERE verified = 1")->fetch_assoc();
							?>
												<p class="card-text" style="font-size: 40px; font-weight: bold;"><?php echo $countverified['totalverified']; ?></p>
							
						  </div>
						 
						<a href="all_users?key=verified" style="text-align: right; padding: 10px; color: white !important;" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
						</div>
					  </div>
					  <div class="col-sm-3">
						<div class="card text-white bg-warning mb-3" >
						
						  <div class="card-body" style="color: white;">
							<h5 class="card-title">Total Active Memes</h5>
							<?php
							$countactivememes = $con->query("SELECT COUNT(*) as totalactivememes FROM memes WHERE is_active = 1")->fetch_assoc();
							?>
												<p class="card-text" style="font-size: 40px; font-weight: bold;"><?php echo $countactivememes['totalactivememes']; ?></p>
							
						  </div>
						 
						<a href="all_memes?key=active" style="text-align: right; padding: 10px; color: white !important;" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
						</div>
					  </div>
					  <div class="col-sm-3">
						<div class="card text-white bg-success mb-3" >
						
						  <div class="card-body" style="color: white;">
							<h5 class="card-title">Total Active Users</h5>
							<?php
							$countactiveusers = $con->query("SELECT COUNT(*) as totalactiveusers FROM users WHERE is_active = 1")->fetch_assoc();
							?>
												<p class="card-text" style="font-size: 40px; font-weight: bold;"><?php echo $countactiveusers['totalactiveusers']; ?></p>
							
						  </div>
						 
						<a href="all_users?key=active" style="text-align: right; padding: 10px; color: white !important;" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
						</div>
					  </div>
						</div>
				</div>
              
			   
			
              <!-- /.card-body -->
            </div>
			
			
            <!-- /.card -->
          </div>
		  <div class="col-sm-6">
		  <div class="card">
              <div class="card-header">
                <h3 class="card-title">Recent Users</h3>
                
              </div>
              <!-- /.card-header -->
			  <div class="card-body">
				<?php
					$getrecent = $con->query("SELECT * FROM users WHERE is_active = 1 ORDER BY created_on DESC LIMIT 5");
					while($hello = $getrecent->fetch_assoc()){
				?>
					<li style="list-style-type: none;">
						<?php echo $hello['fullname']; ?>
						<span style="float: right;">Joined: <?php echo date('d F, Y', strtotime($hello['created_on'])); ?> at <?php echo date('H:i A', strtotime($hello['created_on'])); ?></span>
					</li>
					<hr>
					<?php } ?>
			  </div>
			</div>
			</div>
			 <div class="col-sm-6">
		  <div class="card">
              <div class="card-header">
                <h3 class="card-title">Recent Memes</h3>
                
              </div>
              <!-- /.card-header -->
			  <div class="card-body">
					<?php
					$getrecent = $con->query("SELECT * FROM memes WHERE is_active = 1 ORDER BY posted_on DESC LIMIT 5");
					while($hello = $getrecent->fetch_assoc()){
				?>
					<li style="list-style-type: none;">
						<img src="../../../content/memes/<?php echo $hello['meme_content']; ?>" width="30%" height="30%"/>
						
					</li>
					<hr>
					<?php } ?>
			  </div>
			</div>
			</div>
			
        </div>
		
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- /.control-sidebar -->

<?php include 'footer.php'; ?>
