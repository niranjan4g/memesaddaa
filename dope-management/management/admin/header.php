<?php
   include('../db.php');
        if(isset($_COOKIE['admin']))
        {
            $id=$_COOKIE['admin'];
            
            	$sql = $con->query("SELECT * FROM admin WHERE id='$id'");
              $data = $sql->fetch_array();
              $name=$data['name'];
              $username=$data['username'];
        }
    else{
		$_SESSION['error'] = 'You are not Authorized!';
        header('location:login');
		exit();
    }
  ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->


  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>DOPE MANAGEMENT | MemesAddaa</title>
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="../spoc/plugins/font-awesome/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../spoc/dist/css/adminlte.min.css">
  <link rel="stylesheet" href="../spoc/plugins/color.css">
  <link rel="shortcut icon" href="favicon.png">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.min.js"></script>

</head>
<body class="hold-transition sidebar-mini">

<div class="wrapper">
<?php
        if(isset($_SESSION['error'])){
          echo "
           <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'error',
				layout: 'topRight',
				text: '".$_SESSION['error']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'success',
				layout: 'topRight',
				text: '".$_SESSION['success']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['success']);
        }
      ?>
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index.php" class="nav-link">Home</a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->
      <!-- Notifications Dropdown Menu -->
    </ul>
  </nav>
  <!-- /.navbar -->
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index.php" class="brand-link">
   
      <span class="brand-text font-weight-light">Admin Dashboard</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="../img/default.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $name; ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
	
		<li class="nav-item">
          <a href="index" class="nav-link">
           <i class="nav-icon fa fa-tachometer"></i>
            <p>
              Dashboard
            </p>
          </a>
        </li>
	
		<li class="nav-item">
          <a href="all_users" class="nav-link">
           <i class="nav-icon fa fa-users"></i>
            <p>
              View Users
            </p>
          </a>
        </li>
		<li class="nav-item">
          <a href="all_memes" class="nav-link">
           <i class="nav-icon fa fa-file-o"></i>
            <p>
              All Memes
            </p>
          </a>
        </li>
		
		<li class="nav-item">
          <a href="all_reports" class="nav-link">
           <i class="nav-icon fa fa-exclamation-triangle"></i>
            <p>
              View Reported Memes
            </p>
          </a>
        </li>
		<li class="nav-item">
          <a href="top-meme" class="nav-link">
           <i class="nav-icon fa fa-star"></i>
            <p>
              Top Memes
            </p>
          </a>
        </li>
		<li class="nav-item">
          <a href="top-memer" class="nav-link">
           <i class="nav-icon fa fa-star-o"></i>
            <p>
              Top Memers
            </p>
          </a>
        </li>
		<li class="nav-item">
          <a href="mememonth" class="nav-link">
           <i class="nav-icon fa fa-image"></i>
            <p>
              Memes of the Months
            </p>
          </a>
        </li>
		
		
		
		 
             
	
        <li class="nav-item">
          <a href="c_pass" class="nav-link">
           <i class="nav-icon fa fa-gear"></i>
            <p>
              Change Password
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="logout" class="nav-link">
           <i class="nav-icon fa fa-power-off"></i>
            <p>
              Logout
            </p>
          </a>
        </li>

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
