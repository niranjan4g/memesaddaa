<?php include 'header.php'; ?>
<?php
if (!isset($_GET['page'])) {
					$page = 1;
					} else {
					$page = $_GET['page'];
					}
				?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Top Memes </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index">Home</a></li>
              <li class="breadcrumb-item active">Top Memes</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->

      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
	<?php
        if(isset($_SESSION['error'])){
          echo "
           <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'error',
				layout: 'topRight',
				text: '".$_SESSION['error']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'success',
				layout: 'topRight',
				text: '".$_SESSION['success']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['success']);
        }
      ?>
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
               
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover">
                  <tr>
                    <th>#</th>
                    <th>Meme Id</th>
					<th>Meme</th>
					<th>Meme Caption</th>
					<th>Memer Name</th>
					
					<th>Posted On</th>
                    <th>Active Status</th>
					
					<th>Likes</th>
                  </tr>
                  <tr>
                    <?php
                    $count=1;
					$results_per_page = 10;
					
						$sqli ="SELECT * FROM memes WHERE is_active = 1 ORDER BY likes DESC, posted_on ASC";
					
                    $data = $con->query($sqli);
					$number_of_results = mysqli_num_rows($data);
					$number_of_pages = ceil($number_of_results/$results_per_page);

					
                    $this_page_first_result = ($page-1)*$results_per_page;
					
						$sql='SELECT * FROM memes WHERE is_active = 1 ORDER BY likes DESC, posted_on ASC LIMIT ' . $this_page_first_result . ',' .  $results_per_page;
					
					$result = $con->query($sql);
					if($result->num_rows == 0){ ?>
						<center style="font-size: 20px;">No Memes Found!</center>
						
					<?php }else{
                      while ($row = $result->fetch_assoc()) {
						$user_id = $row['user_id'];
						$getmemer = $con->query("SELECT memername, verified FROM users WHERE user_id = '$user_id'")->fetch_assoc();
                        ?>
                        <td><?php echo$count++ ?></td>
                        <td><?php echo $row['meme_id'] ?></td>
						
						<td><img src="../../../content/memes/<?php echo $row['meme_content']; ?>" width="100px" height="100px" /></td>
						<td><?php echo $row['meme_caption']; ?></td>
						<td><?php echo $getmemer['memername'] ?>&nbsp;<?php if($getmemer['verified'] == 1) { ?><i class="fa fa-check-circle" Style="color: #07bbe8;"></i><?php } ?></td>
						
						<td><?php echo date('d F, Y', strtotime($row['posted_on'])); ?> at <?php echo date('H:i A', strtotime($row['posted_on'])); ?></td>
						<?php
						if($row['is_active'] == 0) {
						?>
						<td>
						<span class="badge badge-danger">Inactive</span>
						</td>
						<?php } else { ?>
						<td>
						<span class="badge badge-success">Active</span>
						</td>
						<?php } ?>
						
						<td>
						<?php echo $row['likes']; ?>
						</td>
							 
                        </tr>
                        <?php
                      }
                    }
                  
                 ?>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div><!-- /.row -->
		<nav aria-label="Page navigation example">
  <ul class="pagination">
    <?php
   $pageid = $page;
   if($pageid != 1)
   {
   ?>
    <li class="page-item"><a class="page-link" href="top-meme?page=<?php echo $pageid-1; ?>">Previous</a></li>
   <?php } else { ?>
   
   <?php }?>
	<?php
	for ($pagei=1;$pagei<=$number_of_pages;$pagei++) {
	?>
    <li class="page-item"><a class="page-link" href="top-meme?page=<?php echo $pagei; ?>"><?php echo $pagei; ?></a></li>
    <?php } ?>
	<?php
   $pageid = $page;
   $pagei = $pagei - 1;
   if($pageid != $pagei){
   ?>
    <li class="page-item"><a class="page-link" href="top-meme?page=<?php echo $pageid+1; ?>">Next</a></li>
   <?php } else { ?>
   
   <?php } ?>
  </ul>
</nav>
<br>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- /.control-sidebar -->

<?php include 'footer.php'; ?>
