<?php include 'header.php'; ?>
<?php
if (!isset($_GET['page'])) {
					$page = 1;
					} else {
					$page = $_GET['page'];
					}
				?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">View Users </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index">Home</a></li>
              <li class="breadcrumb-item active">View Users</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->

      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
	<?php
        if(isset($_SESSION['error'])){
          echo "
           <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'error',
				layout: 'topRight',
				text: '".$_SESSION['error']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'success',
				layout: 'topRight',
				text: '".$_SESSION['success']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['success']);
        }
      ?>
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
               
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover">
                  <tr>
                    <th>#</th>
                    <th>User Id</th>
					<th>Profile Picture</th>
					<th>Memer Name</th>
					<th>Full Name</th>
					<th>Joined On</th>
                    <th>Active Status</th>
					<th>Verified Status</th>
					<th>Action</th>
                  </tr>
                  <tr>
                    <?php
                    $count=1;
					$results_per_page = 10;
					if(isset($_GET['key'])){
						$key = $_GET['key'];
					}else{
						$key = '';
					}
					if($key == 'active') {
						$sqli ="SELECT * FROM users WHERE is_active = 1 ORDER BY userid DESC";
					}else if($key == 'verified'){
						$sqli ="SELECT * FROM users WHERE verified = 1 ORDER BY userid DESC";
					}else{
						$sqli ="SELECT * FROM users ORDER BY userid DESC";
					}
                    $data = $con->query($sqli);
					$number_of_results = mysqli_num_rows($data);
					$number_of_pages = ceil($number_of_results/$results_per_page);

					
                    $this_page_first_result = ($page-1)*$results_per_page;
					if($key == 'active'){
						$sql='SELECT * FROM users WHERE is_active = 1 ORDER BY created_on DESC LIMIT ' . $this_page_first_result . ',' .  $results_per_page;
					}else if($key == 'verified') { 
						$sql='SELECT * FROM users WHERE verified = 1 ORDER BY created_on DESC LIMIT ' . $this_page_first_result . ',' .  $results_per_page;
					}else{
						$sql='SELECT * FROM users ORDER BY created_on DESC LIMIT ' . $this_page_first_result . ',' .  $results_per_page;
					}
					$result = $con->query($sql);
					if($result->num_rows == 0){ ?>
						<center style="font-size: 20px;">No Users Found!</center>
						
					<?php }else{
                      while ($row = $result->fetch_assoc()) {
						
                        ?>
                        <td><?php echo$count++ ?></td>
                        <td><?php echo $row['user_id'] ?></td>
						<?php if($row['profile_photo'] != NULL) { ?>
						<td><img src="../../../profileimages/<?php echo $row['profile_photo']; ?>" width="100px" height="100px" /></td>
						<?php } else { ?>
						<td><img src="../../../assets/user.png" width="100px" height="100px" /></td>
						<?php } ?>
						<td><?php echo $row['memername'] ?></td>
						<td><?php echo $row['fullname']; ?></td>
						<td><?php echo date('d F, Y', strtotime($row['created_on'])); ?> at <?php echo date('H:i A', strtotime($row['created_on'])); ?></td>
						<?php
						if($row['is_active'] == 0) {
						?>
						<td>
						<span class="badge badge-danger">Inactive</span>
						</td>
						<?php } else { ?>
						<td>
						<span class="badge badge-success">Active</span>
						</td>
						<?php } ?>
						<?php
						if($row['verified'] == 0) {
						?>
						<td>
						<span class="badge badge-danger">Not Verified</span>
						</td>
						<?php } else { ?>
						<td>
						<span class="badge badge-info">Verified</span>
						</td>
						<?php } ?>
						<td>
						<a href="edit_user?user_id=<?php echo base64_encode(base64_encode(base64_encode($row['user_id']))); ?>" class="nav-link">
                                <i class="nav-icon fa fa-pencil blue"></i>
                              </a>
						</td>
							 
                        </tr>
                        <?php
                      }
                    }
                  
                 ?>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div><!-- /.row -->
		<nav aria-label="Page navigation example">
  <ul class="pagination">
    <?php
   $pageid = $page;
   if($pageid != 1)
   {
   ?>
    <li class="page-item"><a class="page-link" href="all_users?page=<?php echo $pageid-1; ?>&key=<?php echo $key; ?>">Previous</a></li>
   <?php } else { ?>
   
   <?php }?>
	<?php
	for ($pagei=1;$pagei<=$number_of_pages;$pagei++) {
	?>
    <li class="page-item"><a class="page-link" href="all_users?page=<?php echo $pagei; ?>&key=<?php echo $key; ?>"><?php echo $pagei; ?></a></li>
    <?php } ?>
	<?php
   $pageid = $page;
   $pagei = $pagei - 1;
   if($pageid != $pagei){
   ?>
    <li class="page-item"><a class="page-link" href="all_users?page=<?php echo $pageid+1; ?>&key=<?php echo $key; ?>">Next</a></li>
   <?php } else { ?>
   
   <?php } ?>
  </ul>
</nav>
<br>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- /.control-sidebar -->

<?php include 'footer.php'; ?>
