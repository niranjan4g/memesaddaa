<?php
include '../db.php'; 
if($_SERVER["REQUEST_METHOD"] == "POST"){
	$is_active = $_POST['is_active'];
	$key = $_POST['key'];
	$meme_id = $_POST['meme_id'];
	$adminid = $_COOKIE['admin'];
	
	$res = $con->query("SELECT * FROM memes WHERE meme_id = '$meme_id'");
	if($res->num_rows === 0){
		$_SESSION['error'] = 'No Memes found!';
		header("location: all_memes");
		exit();
	}else{
	  if($key == NULL) {
		$sql = "UPDATE memes SET is_active = '$is_active', updated_on = NOW(), updated_by = '$adminid' WHERE meme_id = '$meme_id'";
		if($con->query($sql) == TRUE){
			$_SESSION['success'] = 'Meme Successfully Updated!';
			header("location: all_memes");
			exit();
		}else{
			$_SESSION['error'] = 'Something Went Wrong! Contact Admin';
			header("location: all_memes");
			exit();
		}
	  }else{
		  $sql = "UPDATE memes SET is_active = '$is_active', updated_on = NOW(), updated_by = '$adminid' WHERE meme_id = '$meme_id'";
			if($con->query($sql) == TRUE){
				$updatereport = $con->query("UPDATE reports SET is_active = 0, action_taken = NOW() WHERE meme_id = '$meme_id'");
			$_SESSION['success'] = 'Meme Successfully Updated!';
			header("location: all_reports");
			exit();
		}else{
			$_SESSION['error'] = 'Something Went Wrong! Contact Admin';
			header("location: all_reports");
			exit();
		}
	  }
	}
}else{
	$_SESSION['error'] = 'You are not Authorized!';
	header("location: index");
	exit();
}
?>