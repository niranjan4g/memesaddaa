<?php include 'header.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Memes of the Months</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index">Home</a></li>
              <li class="breadcrumb-item active">Memes of the Months</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->

      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
	<?php
        if(isset($_SESSION['error'])){
          echo "
           <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'error',
				layout: 'topRight',
				text: '".$_SESSION['error']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'success',
				layout: 'topRight',
				text: '".$_SESSION['success']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['success']);
        }
      ?>
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
               
              </div>
              <!-- /.card-header -->
              <div class="card-body">
				<form method="post">
					<div class="form-group">
						<label>Year</label>
						<select name="year" id="year" class="form-control" required>
							<option value="" selected disabled>--Select Year--</option>
							<option value="2019">2019</option>
							<option value="2020">2020</option>
						</select>
					</div>
					<div class="form-group">
						<label>Month</label>
						<select name="month" id="month" class="form-control" required>
						<option value="" selected disabled>--Select Month--</option>
							<?php for ($m=1; $m<=12; $m++) {
							 $month = date('F', mktime(0,0,0,$m, 1, date('Y')));
							 ?>
							 <option value="<?php echo $month; ?>"><?php echo $month; ?></option>
							 <?php
							 }
							 ?>
						</select>
					</div>
					
					<div class="form-group">
						<button type="submit" id="getmeme" class="btn btn-primary">Get Meme of the Month</button>
					</div>
				</form>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
		  <div class="col-12">
            <div class="card">
             
              <div class="card-body" id="memedata">
				
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div><!-- /.row -->
		
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- /.control-sidebar -->

<?php include 'footer.php'; ?>
<script>
$(document).ready(function(){
	$('#getmeme').on('click', function(e){
		e.preventDefault();
		var year = $('#year').val();
		var month = $('#month').val();
		if(year){
			$.ajax({
				type:'POST',
				url:'getMeme.php',
				data:'year='+year+'&month='+month,
				success:function(data){
					$('#memedata').html(data);
			}
			});
			}else{
					new Noty({
						theme: 'sunset',
						type: 'error',
						layout: 'topRight',
						text: 'Select Year/Month!',
						timeout: 3000
					}).show();
				}
			
	
	});
});
</script>
