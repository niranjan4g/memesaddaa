<?php include 'header.php'; ?>
<?php
if (!isset($_GET['user_id'])) {
	$_SESSION['error'] = 'Select an User First!';
	echo '<script>window.open("all_users","_self");</script>';
	exit();
} else {
	$user_id = base64_decode(base64_decode(base64_decode($_GET['user_id'])));
	$check = $con->query("SELECT * FROM users WHERE user_id = '$user_id'");
	if($check->num_rows === 0){
		$_SESSION['error'] = 'No User Exists!';
		echo '<script>window.open("all_users","_self");</script>';
		exit();
	}else{
		$userdata = $check->fetch_assoc();
	}
}
				?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Edit User</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index">Home</a></li>
              <li class="breadcrumb-item active">Edit User</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->

      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
	<?php
        if(isset($_SESSION['error'])){
          echo "
           <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'error',
				layout: 'topRight',
				text: '".$_SESSION['error']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'success',
				layout: 'topRight',
				text: '".$_SESSION['success']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['success']);
        }
      ?>
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
               
              </div>
              <!-- /.card-header -->
              <div class="card-body">
				<form action="updateUser" method="post">
					<div class="form-group">
						<label>Active</label>
						<select name="is_active" class="form-control" required>
							<?php
							if($userdata['is_active'] == 1) {
							?>
							<option value="1" selected>ACTIVE</option>
							<option value="0">NOT ACTIVE</option>
							<?php } else { ?>
							<option value="0" selected>NOT ACTIVE</option>
							<option value="1">ACTIVE</option>
							<?php } ?>
						</select>
					</div>
					<div class="form-group">
						<label>Verified</label>
						<select name="verified" class="form-control" required>
						<?php
							if($userdata['verified'] == 1) {
							?>
							<option value="1" selected>VERIFIED</option>
							<option value="0">NOT VERIFIED</option>
							<?php } else { ?>
							<option value="0" selected>NOT VERIFIED</option>
							<option value="1">VERIFIED</option>
							<?php } ?>
						</select>
					</div>
					<input type="hidden" name="user_id" value="<?php echo $user_id; ?>" required>
					
					<div class="form-group">
						<button type="submit" class="btn btn-primary">Update</button>
					</div>
				</form>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div><!-- /.row -->
		
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- /.control-sidebar -->

<?php include 'footer.php'; ?>
