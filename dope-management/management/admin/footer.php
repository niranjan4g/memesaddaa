<!-- Main Footer -->
<footer class="main-footer">
  <!-- To the right -->
  <div class="float-right d-none d-sm-inline">

  </div>
  <!-- Default to the left -->
  <strong>Copyright &copy; <?php echo date("Y"); ?> EIT</a>.</strong> All rights reserved.
</footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="../spoc/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../spoc/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../spoc/dist/js/adminlte.min.js"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

</body>
</html>
