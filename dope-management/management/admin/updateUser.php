<?php
include '../db.php'; 
if($_SERVER["REQUEST_METHOD"] == "POST"){
	$is_active = $_POST['is_active'];
	$verified = $_POST['verified'];
	$user_id = $_POST['user_id'];
	$adminid = $_COOKIE['admin'];
	
	$res = $con->query("SELECT * FROM users WHERE user_id = '$user_id'");
	if($res->num_rows === 0){
		$_SESSION['error'] = 'No users found!';
		header("location: all_users");
		exit();
	}else{
		$sql = "UPDATE users SET is_active = '$is_active', verified = '$verified', updated_on = NOW(), updated_by = '$adminid' WHERE user_id = '$user_id'";
		if($con->query($sql) == TRUE){
			$_SESSION['success'] = 'User Successfully Updated!';
			header("location: all_users");
			exit();
		}else{
			$_SESSION['error'] = 'Something Went Wrong! Contact Admin';
			header("location: all_users");
			exit();
		}
	}
}else{
	$_SESSION['error'] = 'You are not Authorized!';
	header("location: index");
	exit();
}
?>