<?php include 'header.php'; ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Total General Registration </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Total General Registration</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->

      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Total General Registration</h3>
                <div class="card-tools">
                  <form  action="helpdesk.php" method="get">
                  <div class="input-group input-group-sm">
                    <input type="text" name="part_search" class="form-control float-right" placeholder="Search" required>
                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                    </div>
                  </div>
               </form>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover">
                  <tr>
                    <th>#</th>
                    <th>G-ID</th>
                    <th>Date</th>
                    <th>Name</th>
                    <th>College</th>
                    <th>Mobile</th>
                    <th>Amount</th>
                    <th>Resceptionist</th>
                  </tr>
                  <tr>
                    <?php
                    $count=1;
                      if (!isset($_GET['part_search'])){
                    $sqli ="SELECT * FROM gen_reg";
                    $data = $con->query($sqli);
                    if($data->num_rows>0){
                      while ($row = $data->fetch_assoc()) {
                        ?>
                        <td><?php echo$count++ ?></td>
                        <td><?php echo $row['gid'] ?></td>
                        <td><?php echo $row['date'] ?></td>
                        <td><?php echo $row['par_name'] ?></td>
                        <td><?php echo $row['par_college'] ?></td>
                        <td><?php echo $row['par_mobile'] ?></td>
                        <td><?php echo $row['package'] ?></td>
                        <td><?php echo $row['res_name'] ?></td>
                      </tr>
                        <?php
                      }
                    }
                  }
                  else{
                    $test_search=$_GET['part_search'];
                    $sqli=("SELECT * FROM gen_reg WHERE  par_name LIKE '%".$test_search."%' OR gid LIKE '%".$test_search."%' OR package LIKE '%".$test_search."%' OR par_college LIKE '%".$test_search."%' OR res_name LIKE '%".$test_search."%' OR par_mobile LIKE '%".$test_search."%'");
                    $data = $con->query($sqli);
                      $count=1;
                      while ($row = $data->fetch_assoc()) {
                        ?>
                        <td><?php echo$count++ ?></td>
                        <td><?php echo $row['gid'] ?></td>
                        <td><?php echo $row['date'] ?></td>
                        <td><?php echo $row['par_name'] ?></td>
                        <td><?php echo $row['par_college'] ?></td>
                        <td><?php echo $row['par_mobile'] ?></td>
                        <td><?php echo $row['package'] ?></td>
                        <td><?php echo $row['res_name'] ?></td>
                        <?php
                      }
                    }
                       ?>
                     </tr>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div><!-- /.row -->
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- /.control-sidebar -->

<?php include 'footer.php'; ?>
