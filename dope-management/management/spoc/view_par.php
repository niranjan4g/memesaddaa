<?php include 'header.php'; ?>
<?php
if (!isset($_GET['id'])) {
  header('Location: index.php');
}
 else{
   include('../db.php');
 $par_id = $_GET['id'];
 $sql = $con->query("SELECT * FROM event  WHERE id='$par_id' ");
 $data = $sql->fetch_array();
}
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Participant details</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Participant details</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">

        <div class="row">
          <div class="col-md-12">
            <!-- Widget: user widget style 2 -->
            <div class="card card-widget widget-user-2">
              <!-- Add the bg color to the header using any of the bg-* classes -->
              <div class="widget-user-header bg-warning">
                <div class="widget-user-image">
                  <img class="img-circle elevation-3" src="../img/default.png" alt="">
                </div>
                <!-- /.widget-user-image -->
                <h3 class="widget-user-username">
                  <?php
                  if ($data['teamname']=="") {
                  }
                  else {
                    echo$data['teamname'];
                  }
                   ?> <br> <?php echo$data['name']; ?></h3>
                <h5 class="widget-user-desc"><?php echo$data['college'];  ?> <br> Payment  <?php
                  if($data['isPaid']==1)
                  {
                  ?><i class="fa fa-check green" aria-hidden="true"></i><?php
                  }
                  else {
                    ?><i class="fa fa-times red" aria-hidden="true"></i><?php
                  }

                 ?> </h5>
              </div>
              <div class="card-footer p-0">
                <ul class="nav flex-column">
                  <?php
                  if ($data['cid1']=="") {
                    // code...
                  }
                  else {
                    ?>
                    <li class="nav-item">
                      <a href="#" class="nav-link">
                        character Id <span class="float-right badge bg-warning"><?php echo $data['cid1']; ?></span>
                      </a>
                    </li>
                    <?php
                  }
                   ?>
                   <?php
                   if ($data['in_game_name1']=="") {
                     // code...
                   }
                   else {
                     ?>
                     <li class="nav-item">
                       <a href="#" class="nav-link">
                        In-game-name <span class="float-right badge bg-warning"><?php echo $data['in_game_name1']; ?></span>
                       </a>
                     </li>
                     <?php
                   }
                    ?>
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Email <span class="float-right badge bg-primary"><?php echo $data['email']; ?></span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Mobile <span class="float-right badge bg-info"><?php echo $data['mobile']; ?></span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Registration ID <span class="float-right badge bg-warning"><?php echo $data['token']; ?></span>
                    </a>
                  </li>
                  <?php
                  if ($data['p1_name']=="") {
                    // code...
                  }
                  else {
                    ?>
                    <li class="nav-item">
                      <a href="#" class="nav-link">
                        1st partner <span class="float-right badge bg-primary"><?php echo $data['p1_name']; ?></span>
                      </a>
                    </li>
                    <?php
                  }
                   ?>
                   <?php
                   if ($data['cid2']=="") {
                     // code...
                   }
                   else {
                     ?>
                     <li class="nav-item">
                       <a href="#" class="nav-link">
                         character Id <span class="float-right badge bg-warning"><?php echo $data['cid2']; ?></span>
                       </a>
                     </li>
                     <?php
                   }
                    ?>
                    <?php
                    if ($data['in_game_name2']=="") {
                      // code...
                    }
                    else {
                      ?>
                      <li class="nav-item">
                        <a href="#" class="nav-link">
                         In-game-name <span class="float-right badge bg-danger"><?php echo $data['in_game_name2']; ?></span>
                        </a>
                      </li>
                      <?php
                    }
                     ?>
                   <?php
                   if ($data['p2_name']=="") {

                   }
                   else {
                     ?>
                     <li class="nav-item">
                       <a href="#" class="nav-link">
                         2nd partner <span class="float-right badge bg-warning"><?php echo $data['p2_name']; ?></span>
                       </a>
                     </li>
                     <?php
                   }
                    ?>

                    <?php
                    if ($data['cid3']=="") {
                      // code...
                    }
                    else {
                      ?>
                      <li class="nav-item">
                        <a href="#" class="nav-link">
                          character Id <span class="float-right badge bg-warning"><?php echo $data['cid3']; ?></span>
                        </a>
                      </li>
                      <?php
                    }
                     ?>
                     <?php
                     if ($data['in_game_name3']=="") {
                       // code...
                     }
                     else {
                       ?>
                       <li class="nav-item">
                         <a href="#" class="nav-link">
                          In-game-name <span class="float-right badge bg-primary"><?php echo $data['in_game_name3']; ?></span>
                         </a>
                       </li>
                       <?php
                     }
                      ?>
                    <?php
                    if ($data['p3_name']=="") {
                      // code...
                    }
                    else {
                      ?>
                      <li class="nav-item">
                        <a href="#" class="nav-link">
                          3rd partner <span class="float-right badge bg-danger"><?php echo $data['p3_name']; ?></span>
                        </a>
                      </li>
                      <?php
                    }
                     ?>
                     <?php
                     if ($data['cid4']=="") {
                       // code...
                     }
                     else {
                       ?>
                       <li class="nav-item">
                         <a href="#" class="nav-link">
                           character Id <span class="float-right badge bg-warning"><?php echo $data['cid4']; ?></span>
                         </a>
                       </li>
                       <?php
                     }
                      ?>
                      <?php
                      if ($data['in_game_name4']=="") {
                        // code...
                      }
                      else {
                        ?>
                        <li class="nav-item">
                          <a href="#" class="nav-link">
                           In-game-name <span class="float-right badge bg-warning"><?php echo $data['in_game_name4']; ?></span>
                          </a>
                        </li>
                        <?php
                      }
                       ?>
                     <?php
                     if ($data['p4_name']=="") {
                       // code...
                     }
                     else {
                       ?>
                       <li class="nav-item">
                         <a href="#" class="nav-link">
                           4th partner <span class="float-right badge bg-warning"><?php echo $data['p4_name']; ?></span>
                         </a>
                       </li>
                       <?php
                     }
                      ?>
                      <?php
                      if ($data['in_game_name5']=="") {
                        // code...
                      }
                      else {
                        ?>
                        <li class="nav-item">
                          <a href="#" class="nav-link">
                           In-game-name <span class="float-right badge bg-primary"><?php echo $data['in_game_name5']; ?></span>
                          </a>
                        </li>
                        <?php
                      }
                       ?>



                </ul>
              </div>
            </div>
            <!-- /.widget-user -->
          </div>
</div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- /.control-sidebar -->

<?php include 'footer.php'; ?>
