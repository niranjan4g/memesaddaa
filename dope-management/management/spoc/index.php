<?php include 'header.php'; ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Registration in <?php echo $name;?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Total Registration</h3>
                <div class="card-tools">
                  <form  action="index.php" method="get">
                  <div class="input-group input-group-sm">
                    <input type="text" name="part_search" class="form-control float-right" placeholder="Search" required>
                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                    </div>
                  </div>
               </form>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover">
                  <tr>
                    <th>#</th>
                    <th>Registration ID</th>
                    <th>Leader Name</th>
                    <th>College</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Payment</th>
                    <th>View</th>
                  </tr>
                  <tr>
                    <?php
                  $c=1;
                      if (!isset($_GET['part_search'])){
                    $sqli ="SELECT * FROM event WHERE event_name='$event'";
                    $data = $con->query($sqli);
                    if($data->num_rows>0){
                      while ($row = $data->fetch_assoc()) {
                        ?>
                        <td><?php echo $c++; ?></td>
                        <td><?php echo $row['token'] ?></td>
                        <td><?php echo $row['name'] ?></td>
                        <td><?php echo $row['college'] ?></td>
                        <td><?php echo $row['email'] ?></td>
                        <td><?php echo $row['mobile'] ?></td>
                      <td>
                          <?php
                            if($row['isPaid']==1)
                            {
                            ?><i class="fa fa-check green" aria-hidden="true"></i><?php
                            }
                            else {
                              ?><i class="fa fa-times red" aria-hidden="true"></i><?php
                            }

                           ?>

                      </td>
                      <td>    <a href="view_par.php?id=<?php echo$row['id'] ?>" class="nav-link">
                            <i class="nav-icon fa fa-angle-double-right blue"></i>
                          </a></td>
                        </tr>
                        <?php
                      }
                    }
                  }
                  else{
                    $test_search=$_GET['part_search'];
                    $sqli=("SELECT * FROM event WHERE  event_name='$event' AND name LIKE '%".$test_search."%' OR token LIKE '%".$test_search."%'");
                    $data = $con->query($sqli);
                      $count=1;
                      while ($row = $data->fetch_assoc()) {
                        ?>
                        <td><?php echo$count++; ?></td>
                        <td><?php echo $row['token'] ?></td>
                        <td><?php echo $row['name'] ?></td>
                        <td><?php echo $row['college'] ?></td>
                        <td><?php echo $row['email'] ?></td>
                        <td><?php echo $row['mobile'] ?></td>
                        <td>
                            <?php
                              if($row['isPaid']==1)
                              {
                              ?><i class="fa fa-check green" aria-hidden="true"></i><?php
                              }
                              else {
                                ?><i class="fa fa-times red" aria-hidden="true"></i><?php
                              }

                             ?>

                        </td>
                        <td>    <a href="view_par.php?id=<?php echo$row['id'] ?>" class="nav-link">
                              <i class="nav-icon fa fa-angle-double-right blue"></i>
                            </a></td>
                        </tr>
                        <?php
                      }
                    }
                       ?>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div><!-- /.row -->
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- /.control-sidebar -->

<?php include 'footer.php'; ?>
