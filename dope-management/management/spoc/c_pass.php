<?php include 'header.php'; ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Setting</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item active">Change Password</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="card card-primary">
           <div class="card-header">
             <h3 class="card-title">Change Password</h3>
           </div>
           <!-- /.card-header -->
           <!-- form start -->
           <form role="form" action="change password.php" method="post">
             <div class="card-body">
               <div class="form-group">
                 <label for="exampleInputPassword1">Password</label>
                 <input type="password" class="form-control" id="exampleInputPassword1" name="password" placeholder="Password"  required >
               </div>
               <div class="form-group">
                 <label for="exampleInputPassword1">Confirm Password</label>
                 <input type="password" class="form-control" id="exampleInputPassword1" name="cpassword" placeholder="Password"  required>
               </div>

             </div>
             <!-- /.card-body -->

             <div class="card-footer">
               <input type="hidden" name="username" value="<?php echo $username; ?>">
               <button type="submit" name="submit" class="btn btn-primary">Submit</button>
             </div>
           </form>
         </div>
        <!-- /.col-md-6 -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- /.control-sidebar -->
<?php include 'footer.php' ?>
