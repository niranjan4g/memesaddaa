<?php
include '../db.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require 'vendor/autoload.php';
date_default_timezone_set("Asia/Kolkata");

if($_SERVER['REQUEST_METHOD'] == "POST"){
	$email = $_POST['email'];
	$sql = "SELECT * FROM vendors WHERE owneremail = '$email'";
	$result = $con->query($sql);
	if($result->num_rows == 0){
		$_SESSION['error'] = 'No Vendor is registered with that email Id';
		header("location: forgot-password.php");
		exit();
	}else{
		$record = $result->fetch_assoc();
		$vendor_id = base64_encode(base64_encode(base64_encode($record['vendor_id'])));
		$password = md5(uniqid(rand(), true));
		$hashed_password = password_hash($password, PASSWORD_DEFAULT);
		
		$res = $con->query("UPDATE vendors SET pwd_link = '$hashed_password' WHERE owneremail = '$email'");
		if($res == TRUE){
			$token = http_build_query(array('id'=>$vendor_id,'key'=>$hashed_password,'code'=>time()));
			$link = 'http://localhost/eit_ecom/management/vendor/resetPassword.php?'.$token;
			
			//code to send mail and execute query
			$mail = new PHPMailer(true);
			
			//Server settings
			$mail->SMTPDebug = 2;                                       // Enable verbose debug output
			$mail->isSMTP();                                            // Set mailer to use SMTP
			$mail->Host       = 'smtp.gmail.com';  						// Specify main and backup SMTP servers
			$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
			$mail->Username   = 'tech.ecommerceeit@gmail.com';               // SMTP username
			$mail->Password   = 'z0Th@n345';                            // SMTP password
			$mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
			$mail->Port       = 587;                                    // TCP port to connect to

			//Recipients
			$mail->setFrom('tech.ecommerceeit@gmail.com');
			$mail->addAddress($email);     						// Add a recipient
			

			// Content
			$mail->isHTML(true);                                  		// Set email format to HTML
			$mail->Subject = 'Password Reset Link';
			$mail->Body    = '<p>Dear Vendor,</p><br><p>Click on this Link to Reset Password <a href="'.$link.'">Reset Password Link</a></p><br><p>The Link will expire in 30 Minutes.</p><p>Do not Share the Mail with anyone, for queries contact Admin</p>';
			//$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

			if($mail->send()){
				if($con->query($sql) == TRUE){
					$_SESSION['success'] = "A Mail with a reset Link has been sent!";
					header("location: login.php");
					exit();
				}else{
					$_SESSION['error'] = "Something went Wrong! Contact Admin";
					header("location: login.php");
					exit();
				}
			}else{
				$_SESSION['error'] = 'We could not send you the Reset Link via email. Please Enter correct email Id or contact Admin';
				header("location: login.php");
				exit();
			}
		}else{
			$_SESSION['error'] = "Something went Wrong! Contact Admin";
					header("location: login.php");
					exit();
		}
	}
}
?>