<?php
   include('../db.php');
 if(isset($_SESSION['vendor']))
{
	$_SESSION['success'] = 'Logged in Successfully!';
    header('location:index.php');
	exit();
}
?>
<?php
if(isset($_GET)){
	$vendor_id = htmlspecialchars(base64_decode(base64_decode(base64_decode($_GET['id']))));
	$link_url = htmlspecialchars($_GET['key']);
	$click_time = htmlspecialchars($_GET['code']);
	$current_time = time();
	$diff = $current_time - $click_time;
	if($diff < 4400){
		$result = $con->query("SELECT pwd_link FROM vendors WHERE vendor_id = '$vendor_id'");
		if($result->num_rows > 0){
			$row = $result->fetch_assoc();
			$pwd_link = $row['pwd_link'];
			
			if($pwd_link === $link_url){
				$qry = $con->query("UPDATE vendors SET pwd_link = '' WHERE vendor_id = '$vendor_id'");
			}else{
				
			?>
			<script>
			alert("Something Went Wrong! contact admin");
			window.open("login.php","_self");
			</script>
			<?php
			}
		}else{ 
		
		?>
			<script>
			alert("You are not authorized!");
			window.open("login.php","_self");
			</script>
		<?php }
	}else{
		
		?>
		<script>
		alert("Link has Expired! Try Again");
			window.open("forgot-password.php","_self");
			</script>
		<?php
	}
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Studio Bazar</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../spoc/dist/css/adminlte.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../spoc/plugins/iCheck/square/blue.css">
<link rel="shortcut icon" href="logo.jpg">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.min.js"></script>
</head>
<body class="hold-transition login-page">

<div class="login-box">
  <div class="login-logo">
    <a href=""><b>Studio Bazar </b></a>
  </div>
  <?php
        if(isset($_SESSION['error'])){
          echo "
           <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'error',
				layout: 'topRight',
				text: '".$_SESSION['error']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'success',
				layout: 'topRight',
				text: '".$_SESSION['success']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['success']);
        }
      ?>
	 
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <h4 class="login-box-msg">Vendor Login</h4>

      <form action="updatePassword.php" method="post">
        <div class="form-group has-feedback">
          <input type="password" class="form-control" name="password" placeholder="Enter Password">
        </div>
        <div class="form-group has-feedback">
          <input type="password" class="form-control" name="cpassword"placeholder="Confirm Password">
        </div>
		<input type="hidden" name="vendor_id" value="<?php echo $vendor_id; ?>">
        <div class="row">
        
          <!-- /.col -->
          <div class="col-12">
            <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat">Change Password</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
	  
     
      <!-- /.social-auth-links -->
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="../spoc/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../spoc/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- iCheck -->
<script src="../spoc/plugins/iCheck/icheck.min.js"></script>

</body>
</html>
