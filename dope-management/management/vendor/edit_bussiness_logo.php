<?php
include '../db.php';
if(!isset($_SESSION['vendor'])){
	$_SESSION['error'] = 'You are not authorized! Please Login!';
	header("location: login.php");
	exit();
}else{
	if($_SERVER["REQUEST_METHOD"] == "POST"){
		
		$target_dir = "../../uploaded_files/bussinesslogo/"; //directory to store category images
		$target_file = basename($_FILES["bussinesslogo"]["name"]);
		$uploadOk = 1;
		
		if(empty($target_file)){
					$_SESSION['error'] = "Please Upload Bussiness Logo";
					header("location: view_profile.php");
					exit();
			}else{
				$random = rand(0,999999999);
				$name_new = ($random.$target_file);
				$imageFileType = strtolower(pathinfo($name_new,PATHINFO_EXTENSION));
					
				if ($_FILES["bussinesslogo"]["size"] > 500000) {
					$_SESSION['error'] = "Sorry, your file is too large.";
					$uploadOk = 0;
					header("location: view_profile.php");
					exit();
				}
				if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
					$_SESSION['error'] = "Sorry, only JPG, JPEG & PNG files are allowed.";
					$uploadOk = 0;
					header("location: view_profile.php");
					exit();
				}
				if ($uploadOk == 0) {
					$_SESSION['error'] = "Sorry, your Document was not uploaded! Contact Admin.";
					header("location: view_profile.php");
					exit();
				} else {
					if (move_uploaded_file($_FILES["bussinesslogo"]["tmp_name"], $target_dir.$name_new)) {
						$_SESSION['succcess'] = "Bussiness Logo was Successfully Uploaded";
					} else {
						$_SESSION['error'] = "Sorry, there was an error uploading your Bussiness Logo.";
						header("location: view_profile.php");
						exit();
					}
				}
				$bussinesslogo = $name_new;
			}
		$vendor_id = $_POST['vendor_id'];
		
		$sql = "UPDATE vendors SET bussinesslogo = '$bussinesslogo', updated_by = '$vendor_id', updated_on = NOW() WHERE vendor_id = '$vendor_id'";
		if($con->query($sql) == TRUE){
			$_SESSION['success'] = 'Bussiness Logo Updated Successfully!';
			header("location: view_profile.php");
			exit();
		}else{
			$_SESSION['error'] = 'Failed! Contact Developer';
			header("location: view_profile.php");
			exit();
		}
	}else{
		$_SESSION['error'] = 'Not Authorized';
		header("location: view_profile.php");
		exit();
	}
}
?>