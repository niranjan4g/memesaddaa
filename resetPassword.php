<?php include 'includes/header.php'; ?>
<?php
if(isset($_COOKIE['user'])){
	$_SESSION['success'] = 'Logged In Successfully';
	header("location: index");
	exit();
}
?>
<?php
if(isset($_GET)){
	$user_id = htmlspecialchars(base64_decode(base64_decode(base64_decode($_GET['id']))));
	$link_url = htmlspecialchars($_GET['key']);
	$click_time = htmlspecialchars($_GET['code']);
	$current_time = time();
	$diff = $current_time - $click_time;
	if($diff < 4400){
		$result = $con->query("SELECT pwd_link FROM users WHERE user_id = '$user_id'");
		if($result->num_rows > 0){
			$row = $result->fetch_assoc();
			$pwd_link = $row['pwd_link'];
			
			if($pwd_link === $link_url){
				$qry = $con->query("UPDATE users SET pwd_link = '' WHERE user_id = '$user_id'");
			}else{
				$_SESSION['error'] = 'Something Went Wrong! contact admin';
			?>
			<script>
			
			window.open("index","_self");
			</script>
			<?php
			}
		}else{ 
		 $_SESSION['error'] = 'You are not authorized!';
		?>
			<script>
			
			window.open("index","_self");
			</script>
		<?php }
	}else{
		$_SESSION['error'] = 'Link has Expired! Try Again';
		?>
		<script>
		
			window.open("forgot-password","_self");
			</script>
		<?php
	}
}else{
	$_SESSION['error'] = 'You are not Authorized!';
	?>
	<script>
	window.open("login","_self");
	</script>
	<?php
	
}
?>
<body class="bg-transparent">
<?php
        if(isset($_SESSION['error'])){
          echo "
           <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'error',
				layout: 'topRight',
				text: '".$_SESSION['error']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'success',
				layout: 'topRight',
				text: '".$_SESSION['success']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['success']);
        }
      ?>
    <main>
        <div class="main-wrapper pb-0 mb-0">
            <div class="timeline-wrapper">
                <div class="timeline-header">
                    <div class="container-fluid p-0" style="margin-top: 40px;">
                        <div class="row">
                            <div class="col-lg-12">
                               
									<center>
                                        <a href="index">
                                            <img src="assets/images/logo/logo.png" alt="timeline logo">
                                        </a>
									</center>
                                   
                            </div>
                            
                    </div>
                </div>
                <div class="timeline-page-wrapper" style="margin-top: 80px;">
                    <div class="container-fluid p-0">
                        <div class="row no-gutters">
                           
                            <div class="col-lg-12 order-1 order-lg-2 d-flex align-items-center justify-content-center">
                                <div class="signup-form-wrapper">
                                   
                                    <div class="signup-inner text-center">
                                        <h3 class="title">Reset Password</h3>
                                        <form class="signup-inner--form" method="post" action="updatePassword">
                                            <div class="row">
                                                <div class="col-12">
                                                    <input type="password" name="password" id="password" class="single-field" placeholder="Enter Password" required>
													
                                                </div>
												 <div class="col-12">
                                                    <input type="password" name="cpassword" id="cpassword" class="single-field" placeholder="Confirm Password" required>
													
                                                </div>
												<input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
                                                <p id="match-error"></p>
                                               
                                                <div class="col-12">
                                                    <button class="submit-btn" id="changep">Change Password</button>
                                                </div>
                                            </div>
                                            
                                        </form>
										
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <!-- JS
============================================ -->

    <!-- Modernizer JS -->
    <script src="assets/js/vendor/modernizr-3.6.0.min.js"></script>
    <!-- jQuery JS -->
    <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
    <!-- Popper JS -->
    <script src="assets/js/vendor/popper.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="assets/js/vendor/bootstrap.min.js"></script>
    <!-- Slick Slider JS -->
    <script src="assets/js/plugins/slick.min.js"></script>
    <!-- nice select JS -->
    <script src="assets/js/plugins/nice-select.min.js"></script>
    <!-- audio video player JS -->
    <script src="assets/js/plugins/plyr.min.js"></script>
    <!-- perfect scrollbar js -->
    <script src="assets/js/plugins/perfect-scrollbar.min.js"></script>
    <!-- light gallery js -->
    <script src="assets/js/plugins/lightgallery-all.min.js"></script>
    <!-- image loaded js -->
    <script src="assets/js/plugins/imagesloaded.pkgd.min.js"></script>
    <!-- isotope filter js -->
    <script src="assets/js/plugins/isotope.pkgd.min.js"></script>
    <!-- Main JS -->
    <script src="assets/js/main.js"></script>
	<script>
	$('#password, #cpassword').on('keyup', function() {
	  if ($('#password').val() == $('#cpassword').val()) {
		$('#match-error').html('Passwords Matching').css('color', 'green');
		$('#changep').prop('disabled', false);
	  } else {
		$('#match-error').html('Password do Not Matching').css('color', 'red');
		$('#changep').prop('disabled', true);
	  }
	});
	</script>
</body>

</html>