<?php include 'includes/header.php'; ?>
<body>

<?php
        if(isset($_SESSION['error'])){
          echo "
           <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'error',
				layout: 'topRight',
				text: '".$_SESSION['error']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'success',
				layout: 'topRight',
				text: '".$_SESSION['success']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['success']);
        }
      ?>
    <!-- header area start -->
    <?php include 'includes/brandbar.php'; ?>
    <!-- header area end -->
    <!-- header area start -->
    <?php include 'includes/mobilenav.php'; ?>
    <!-- header area end -->

    <main>
		  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header" style="height: 80px;">
      
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
	<?php
        if(isset($_SESSION['error'])){
          echo "
           <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'error',
				layout: 'topRight',
				text: '".$_SESSION['error']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'success',
				layout: 'topRight',
				text: '".$_SESSION['success']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['success']);
        }
      ?>
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header" style="background-color: black; ">
                <h3 class="card-title" style="text-align: center; color: white;">TOP 10 MEMES OF <?php 
					$todays_date = date('d');
				  $current_month = date('F');
				  $current_year = date('Y');
				  if($todays_date < 5){
					  echo strtoupper(date('F', strtotime('-1 months')));
				  }else{
					  echo strtoupper(date('F'));
				  }
					
				?></h3>
				
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover" style="text-align: center;">
                  <tr>
                    <th>Position</th>
                    <th>Meme</th>
					<th>Memer Name</th>
					<th>Total Likes</th>
					<th>Posted On</th>
                   
                  </tr>
				  <?php
				  
				  if($todays_date < 5){
					  $lastmonth = date('F', strtotime('-1 months'));
					  $sqli = "SELECT * FROM memes WHERE is_active = 1 AND month = '$lastmonth' AND year = '$current_year' ORDER BY likes DESC, posted_on ASC LIMIT 10";
				  }else{
					  $sqli = "SELECT * FROM memes WHERE is_active = 1 AND month = '$current_month' AND year = '$current_year' ORDER BY likes DESC, posted_on ASC LIMIT 10";
				  }
				  $run_query = $con->query($sqli);
				  if($run_query->num_rows === 0){
					  echo "No Memes Found!";
				  }else{
					  $count = 0;
					  while($row = $run_query->fetch_assoc()){
							$pos = $count + 1;
							$user_id = $row['user_id'];
							$getmemer = $con->query("SELECT memername, verified FROM users WHERE user_id = '$user_id'")->fetch_assoc();
				  ?>
                  <tr>
						<?php if($pos == 1) { ?>
						<td><i class="fa fa-trophy" style="color: gold; font-size: 40px;"></i></td>
						<?php } else if($pos == 2) { ?>
						<td><i class="fa fa-trophy" style="color: silver; font-size: 30px;"></i></td>
						<?php } else if($pos == 3) { ?>
						<td><i class="fa fa-trophy" style="color: saddlebrown; font-size: 20px;"></i></td>
						<?php } else { ?>
						<td><?php echo $pos; ?></td>
						<?php } ?>
                        <td><img id="memeimage<?php echo $pos; ?>" src="content/memes/<?php echo $row['meme_content']; ?>" width="200px" /></td>
						<td><a href="profile?user_id=<?php echo base64_encode(base64_encode(base64_encode($user_id))); ?>">
							@<?php echo $getmemer['memername']; ?>
							<?php if($getmemer['verified'] == 1){ ?>
								<i class="fa fa-check-circle" style="color: #07bbe8;"></i>
							<?php } ?>
						</a>
						</td>
						<td><?php echo $row['likes']; ?> Likes</td>
						<td><?php echo date('d F, Y', strtotime($row['posted_on'])); ?> at <?php echo date('H:i A', strtotime($row['posted_on'])); ?></td>
						
                  </tr>
				  <?php $count++; } }?>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
			<?php
			if($todays_date == 5){
				$topmonth = date('F', strtotime('-1 months'));
				$check_presence = $con->query("SELECT * FROM mememonth WHERE month = '$topmonth' AND year = '$current_year'");
				if($check_presence->num_rows === 0){
					$gettopmeme = $con->query("SELECT * FROM memes WHERE month = '$topmonth' AND year = '$current_year' AND is_active = 1 ORDER BY likes DESC, posted_on ASC LIMIT 1");
					if($gettopmeme->num_rows != 0){
						$memedata = $gettopmeme->fetch_assoc();
						$meme_id = $memedata['meme_id'];
						$addtopmeme = $con->query("INSERT INTO mememonth (meme_id, month, year, declared_on) WHERE ('$meme_id', '$topmonth', '$current_year', NOW())");
					}
				}
			}
			?>
			<div class="card">
              <div class="card-header" style="background-color: black; ">
                <h3 class="card-title" style="text-align: center; color: white;">MEME OF <?php
					if($todays_date < 5) {
						$last1month = date('F', strtotime('-2 months'));
						echo strtoupper($last1month);
					}else{
						$last1month = date('F', strtotime('-1 months'));
						echo strtoupper($last1month);
					}
				
				?>
				MONTH</h3>
				
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0" >
			  <?php 
			  if($todays_date < 5){
				  $last2month = date('F', strtotime('-2 months'));
				  $getwinner = $con->query("SELECT * FROM mememonth WHERE month = '$last2month' AND year = '$current_year'")->fetch_assoc();
				  $winnermemeid = $getwinner['meme_id'];
				  $memewinnerdata = $con->query("SELECT * FROM memes WHERE meme_id = '$winnermemeid'")->fetch_assoc();
				  $winnerid = $memewinnerdata['user_id'];
				  $winnerdata = $con->query("SELECT user_id, fullname, memername, verified FROM users WHERE user_id = '$winnerid'")->fetch_assoc();
			  }else{
				  $last2month = date('F', strtotime('-1 months'));
				  $getwinner = $con->query("SELECT * FROM mememonth WHERE month = '$last2month' AND year = '$current_year'")->fetch_assoc();
				  $winnermemeid = $getwinner['meme_id'];
				  $memewinnerdata = $con->query("SELECT * FROM memes WHERE meme_id = '$winnermemeid'")->fetch_assoc();
				  $winnerid = $memewinnerdata['user_id'];
				  $winnerdata = $con->query("SELECT user_id, fullname, memername, verified FROM users WHERE user_id = '$winnerid'")->fetch_assoc();
			  }
			  ?>
				  <div class="col-12">
					<?php
					if($memewinnerdata['meme_content'] == NULL){
					?>
						<img src="assets/default-result.jpg" style="margin-left: 2%; margin-right: 2%; margin-top: 2%;" />
					<?php } else { ?>
						<img src="content/memes/<?php echo $memewinnerdata['meme_content']; ?>" style="margin-left: 2%; margin-right: 2%; margin-top: 2%;" />
					<?php } ?>
				  </div>
				  <?php if($memewinnerdata['meme_content'] == NULL){ ?>
				   <div class="col-12" style="margin: 4%;">
					
					
				  </div>
				  <?php } else {?>
				  <div class="col-12" style="margin: 4%; height: 280px;">
					<h1>Congratulations, <?php echo $winnerdata['fullname']; ?></h1>
					<h4><a href="profile?user_id=<?php echo base64_encode(base64_encode(base64_encode($winnerid))); ?>">
						@<?php echo $winnerdata['memername']; ?><?php if($winnerdata['verified'] == 1) {?>
							<i class="fa fa-check-circle" style="color: #07bbe8;"></i>
						<?php } ?>
					</a>, won the Meme of the Month Award of a 5$ Google Play Gift Card.</h4>
				  </div>
				  <?php } ?>
            </div>
            <!-- /.card -->
          </div>
        </div><!-- /.row -->
	
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
	
	</main>
	<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			
		  </div>
		  <div class="modal-body">
			<img src="" id="imagepreview" style="width: 100%; height: 100%;" >
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
    <!-- Scroll to top start -->
    <div class="scroll-top not-visible">
        <i class="bi bi-finger-index"></i>
    </div>
	
 <?php include 'includes/footer.php'; ?>
 <script>
	$("#memeimage1").on("click", function() {
	   $('#imagepreview').attr('src', $('#memeimage1').attr('src')); 
	   $('#imagemodal').modal('show');
	});
	$("#memeimage2").on("click", function() {
	   $('#imagepreview').attr('src', $('#memeimage2').attr('src')); 
	   $('#imagemodal').modal('show');
	});
	$("#memeimage3").on("click", function() {
	   $('#imagepreview').attr('src', $('#memeimage3').attr('src')); 
	   $('#imagemodal').modal('show');
	});
	$("#memeimage4").on("click", function() {
	   $('#imagepreview').attr('src', $('#memeimage4').attr('src'));
	   $('#imagemodal').modal('show'); 
	});
	$("#memeimage5").on("click", function() {
	   $('#imagepreview').attr('src', $('#memeimage5').attr('src')); 
	   $('#imagemodal').modal('show'); 
	});
	$("#memeimage6").on("click", function() {
	   $('#imagepreview').attr('src', $('#memeimage6').attr('src')); 
	   $('#imagemodal').modal('show'); 
	});
	$("#memeimage7").on("click", function() {
	   $('#imagepreview').attr('src', $('#memeimage7').attr('src')); 
	   $('#imagemodal').modal('show'); 
	});
	$("#memeimage8").on("click", function() {
	   $('#imagepreview').attr('src', $('#memeimage8').attr('src')); 
	   $('#imagemodal').modal('show');
	});
	$("#memeimage9").on("click", function() {
	   $('#imagepreview').attr('src', $('#memeimage9').attr('src')); 
	   $('#imagemodal').modal('show'); 
	});
	$("#memeimage10").on("click", function() {
	   $('#imagepreview').attr('src', $('#memeimage10').attr('src')); 
	   $('#imagemodal').modal('show');
	});
</script>
