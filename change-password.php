<?php
include 'includes/config.php'; 

	if($_SERVER["REQUEST_METHOD"] == "POST"){
		$oldpassword = $_POST["cpass"];
		$password = $_POST["npass"];
		$cpassword = $_POST["rpass"];
		
		$user_id = $_COOKIE['user'];
		$sql = "SELECT password FROM users WHERE user_id = '$user_id'";
		//$hashpassword = password_hash($oldpassword, PASSWORD_DEFAULT);
		$result = $con->query($sql);
		$record = $result->fetch_assoc();
		$fetchedpassword = $record["password"];
		$user = base64_encode(base64_encode(base64_encode($user_id)));
		
		if(password_verify($oldpassword, $fetchedpassword)){
			if($password == $cpassword){
				$hashedpassword = password_hash($password, PASSWORD_DEFAULT);
				$query = "UPDATE users SET password = '$hashedpassword' WHERE user_id = '$user_id'";
				if($con->query($query) == TRUE){
					$_SESSION['success'] = "Password Updated Successfully!";
					header("location: profile?user_id=$user");
					exit();
				}else{
					$_SESSION['error'] = "Password wasn't updated! Contact Admin";
					header("location: profile?user_id=$user");
					exit();
				}
			}else{
				$_SESSION['error'] = "Passwords do not Match!";
				header("location: profile?user_id=$user");
				exit();
			}
		}else{
			$_SESSION['error'] = "Enter Current Password Correctly!";
			header("location: profile?user_id=$user");
			exit();
		}
	}else{
	$_SESSION['error'] = "You are unauthorized! Please Login";
	header("location: index");
	exit();
}
?>