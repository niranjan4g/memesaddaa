<?php
include 'includes/config.php';

if($_SERVER['REQUEST_METHOD'] == "POST") {
	$user_id = $_COOKIE['user'];
	$user = base64_encode(base64_encode(base64_encode($user_id)));
    if(is_array($_FILES)) {


        $file = $_FILES['profile_photo']['tmp_name']; 
        $sourceProperties = getimagesize($file);
        $fileNewName = time();
        $folderPath = "profileimages/";
        $ext = pathinfo($_FILES['profile_photo']['name'], PATHINFO_EXTENSION);
        $imageType = $sourceProperties[2];


        switch ($imageType) {


            case IMAGETYPE_PNG:
                $imageResourceId = imagecreatefrompng($file); 
                $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                imagepng($targetLayer,$folderPath. $fileNewName. "_thump.". $ext);
                break;

            case IMAGETYPE_JPEG:
                $imageResourceId = imagecreatefromjpeg($file); 
                $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                imagejpeg($targetLayer,$folderPath. $fileNewName. "_thump.". $ext);
                break;


            default:
                $_SESSION['error'] = "Invalid Image type.";
				header("location: profile?user_id=$user");
                exit();
                break;
        }


        if(move_uploaded_file($file, $folderPath. $fileNewName. ".". $ext)){
			
			$finalpath = $fileNewName."_thump.".$ext;
			$sql = "UPDATE users SET profile_photo = '$finalpath' WHERE user_id = '$user_id'";
			if($con->query($sql) == TRUE){
				$_SESSION['success'] = 'Profile Picture Updated Successfully';
				header("location: profile?user_id=$user");
				exit();
			}else{
				$_SESSION['error'] = 'Something went Wrong! Contact Admin';
				header("location: profile?user_id=$user");
				exit();
			}
		}else{
			$_SESSION['error'] = 'Something went Wrong! Contact Admin';
			header("location: profile?user_id=$user");
			exit();
		}
    }
}else{
	$_SESSION['error'] = 'You are not Authorized!';
	header("location: index");
	exit();
}


function imageResize($imageResourceId,$width,$height) {


    $targetWidth =256;
    $targetHeight =256;


    $targetLayer=imagecreatetruecolor($targetWidth,$targetHeight);
    imagecopyresampled($targetLayer,$imageResourceId,0,0,0,0,$targetWidth,$targetHeight, $width,$height);


    return $targetLayer;
}
?>