<?php
include 'includes/config.php';
if($_SERVER["REQUEST_METHOD"] == "POST"){
	$email = $con->real_escape_string($_POST['email']);
	$password = $con->real_escape_string($_POST['password']);
	if ($email == "" || $password == "")
	{
		$_SESSION['error'] = "Please Enter Email/Password";
		header("location: login");
		exit();
	}else{
		$sql = $con->query("SELECT * FROM users WHERE email='$email' AND is_active = 1");
		if ($sql->num_rows > 0) {
            $data = $sql->fetch_array();
			if($data['is_active'] == 0){
				$_SESSION['error'] = 'You are not Verified! Check your mail and click the link to Verify';
				header("location: login");
				exit();
			}else{
				if (password_verify($password, $data['password'])) {
					$user_id=$data['user_id'];
			  
					setcookie("user", $data["user_id"], time()+3600);
					$_SESSION['success'] = "Logged In Successfully";
					header('location: index');
				}
				else {
					$_SESSION['error'] = "Email/Password is incorrect";
					header('location:login');
				}
			}
		}
		else{
			$_SESSION['error'] = "User not Registered! Please Register";
			header("location: login");
		}
	}
}else{
	$_SESSION['error'] = 'You are unauthorized!';
	header("location: login");
	exit();
}
?>